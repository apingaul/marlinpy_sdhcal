#!/usr/bin/env python
''' Small utility to insert files into db
    To register files in the db already uploaded to the grid you still need to use the --gridUpload switch:
    Upload will obviously fail since file already present on the grid but the script will nonetheless register the file in the db
'''
# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
import six

import os
import argparse
import platform
import json
import time
import subprocess

from marlinpy_sdhcal.utils import helper
from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)

try:
    from marlinpy_sdhcal.utils import gridUtils as gdu
except ImportError:
    logger.warning(
        "Could not import Dirac, check your PYTHONPATH or source dirac env file if you plan on running on the grid")


# -----------------------------------------------------------------------------
def insertFile(db, file, run_number, file_type, location_type, check_path=True):
    ''' Returns True/False
    '''
    files_indb_list = db.getFilesFromRun(run_number=run_number, file_type=file_type, location_type=location_type)
    if files_indb_list:
        for f in files_indb_list:
            if f == file:
                logger.warning("File '%s' already in db, not adding it" % f)
                continue
        logger.warning("There are already '%s' files associated with run '%s' in the db: " % (file_type, run_number))
        logger.warning(files_indb_list)

    ans = db.insertRunFile(run_number=run_number,
                           path_to_file=file,
                           file_type=file_type,
                           location_type=location_type,
                           check_path=check_path,
                           commit=True)
    if ans is not None and ans != 1:  # insertRunFile returns the number of file uploaded to the db, should be 1 or None in this case
        logger.error("Weird number of db entry modified: got '%s', expected 1" % ans)
        return False
    return True


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def main(args=None):
    parser = argparse.ArgumentParser(
        description=
        'insert path to files into db. Pass --runList 0 if you want to process every runs from every testbeams. Files are inserted into the db only if the path is valid (i.e. the file is found by the script)'
        'If you provide --gridUpload switch it will also try to upload file to the grid to folder defined in --gridPath',
        prog=__file__)
    parser.add_argument('--db_conf', required=True, help='Path to json file with db parameters')
    parser.add_argument(
        '--format',
        required=True,
        help=
        'File format (can be local or on (Dirac) grid), requires {tb_name} and {run_number}! eg. /eos/project/s/sdhcal/data/{tb_name}/Raw/DHCAL_{run_number}_I0_0.slcio'
    )
    parser.add_argument(
        '--location',
        required=True,
        help=
        'Where the file is stored (eos_sdhcal, local, grid, etc.) Corresponds to an entry in `files_locations_types` table in the db'
    )
    parser.add_argument(
        '--type',
        required=True,
        help='Type of the file (raw, trivent, etc.) Corresponds to an entry in `analysis_versions` table in the db')
    parser.add_argument(
        '--runList',
        required=True,
        nargs='+',
        type=int,
        help=
        'List of run number separated by a space. To process every run with the given format use --runList 0. Note that only runs present in the db will be processed!'
    )
    parser.add_argument(
        '--gridUpload',
        action='store_true',
        default=False,
        help='Upload the local file to the (dirac) grid, use --gridPath to pass the path to use on the grid')
    parser.add_argument(
        '--gridPath',
        help=
        'Path to folder on the (dirac) grid. Can use {tb_name} in the name. e.g.: /calice/users/a/apingaul/data/{tb_name}/Raw. Only used with --gridUpload'
    )
    parser.add_argument('--SE', default='IN2P3-USER', help='Storage Element to use')
    args = parser.parse_args()

    if args.gridUpload and args.gridPath is None:
        parser.error("--gridUpload requires you set --gridPath")

    db = dbu(db_conf=args.db_conf)
    if args.runList[0] == 0:
        testbeams = db.executeCmd('SELECT `idx`,`name` FROM `testbeams`')

    else:
        testbeams = []
        tbIdxSet = set()
        for run in args.runList:
            tb_idx = db.getTestBeamIdxFromRun(run)
            if not tb_idx in tbIdxSet:
                tbIdxSet.add(tb_idx)
                testbeams.append({"idx": tb_idx, 'name': db.getTestBeamNameFromRun(run)})
    ''' results will hold list of files the exec tried to update and the status
        grid_* are only added if trying to upload to the grid
        db_status = True means the file is in the db
        grid_status = True means the file was uploaded to the grid
        If upload fails because the file is already present, then grid_status will be set to False but db_status can still be True

        eg = {
            "730677": {
                "DHCAL_730677_I0_0.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_0.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_0.slcio"
                },
                "DHCAL_730677_I0_1.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_1.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_1.slcio"
                },
                "DHCAL_730677_I0_2.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_2.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_2.slcio"
                },
            }
        }
    '''

    results = {}
    path = args.format
    try:
        for tb in testbeams:
            tb_idx = tb['idx']
            tb_name = tb['name']
            runList = db.getRunList({'tb_idx': tb_idx})
            if args.runList[0] != 0:
                # keep only runs from args.runList AND present in this tb
                runList = list(set(runList) & set(args.runList))

            if not runList:
                logger.error("Could not generate runList for tb '%s', check your parameters" % tb_name)
                continue

            for run_number in runList:
                results[str(run_number)] = {}

                try:
                    file_format = path.format(tb_name=tb_name, run_number=run_number)
                    if 'grid' in args.location and not args.gridUpload:
                        # Register a file already on the grid
                        file_list = helper.findAllInputFiles([file_format], run_number, isOnGrid=True)
                    else:
                        file_list = helper.findAllInputFiles([file_format], run_number)
                except OSError as e:
                    logger.error(e)
                    continue

                if not file_list:
                    errMsg = "No files found for run '%s' with type %s" % (run_number, args.type)
                    results[str(run_number)] = errMsg
                    logger.error(errMsg)
                    continue

                logger.debug("List of files to insert into db for run " + str(run_number) + ': ' + str(file_list))
                for f in file_list:
                    fName = os.path.basename(f)
                    results[str(run_number)][fName] = {"db_status": False, "local_path": f}
                    logger.debug(" Inserting %s..." % str(f))
                    if args.gridUpload is False:
                        if 'grid' in args.location:
                            is_Ok = insertFile(db, f, run_number, args.type, args.location, check_path=False)
                        else:
                            is_Ok = insertFile(db, f, run_number, args.type, args.location)
                        results[str(run_number)][fName]['db_status'] = is_Ok
                    else:
                        if "{tb_name}" in args.gridPath:
                            p = args.gridPath.format(tb_name=tb_name)
                        else:
                            p = args.gridPath

                        grid_file = '{}/{}'.format(p, fName)
                        logger.debug(" Path to grid file: %s" % grid_file)
                        results[str(run_number)][fName]['grid_path'] = grid_file
                        results[str(run_number)][fName]['grid_status'] = False

                        # Check if file already exists
                        if gdu.findFileOnGrid(grid_file)[0]:
                            logger.error(
                                "File '%s' already exists on the grid, if you want to update it you need to remove it beforehand, skipping..."
                                % grid_file)
                            # File already present on grid, make sure it is also in the db
                            is_Ok = insertFile(db, grid_file, run_number, args.type, args.location, check_path=False)
                            results[str(run_number)][fName]['db_status'] = is_Ok
                            results[str(run_number)][fName]['grid_failed'] = "Already present"
                            continue

                        cmd = 'source /cvmfs/clicdp.cern.ch/DIRAC/bashrc;'
                        cmd += "dirac-dms-add-file {grid_file} {local_file} {SE}".format(local_file=f,
                                                                                         grid_file=grid_file,
                                                                                         SE=args.SE)
                        logger.debug("Upload command: '%s'" % cmd)
                        try:
                            proc = subprocess.Popen(cmd,
                                                    stdout=subprocess.PIPE,
                                                    stderr=subprocess.STDOUT,
                                                    shell=True,
                                                    universal_newlines=True)
                            logger.debug("\n\n Answer: ")
                            output, _ = proc.communicate()
                            for line in output.split('\n'):
                                logger.info(line)
                            if output.split('\n')[-2] != "Successfully uploaded file to %s" % args.SE:
                                logger.error("Failed to upload file '%s' with cmd '%s' " % (grid_file, cmd))
                                continue
                        except subprocess.CalledProcessError:
                            logger.error("Failed to upload file with cmd '%s' " % cmd)

                        #Check everything went fine
                        if not gdu.findFileOnGrid(grid_file)[0]:
                            logger.error('Could not find the file on the grid, upload probably failed')
                            continue

                        results[str(run_number)][fName]['grid_status'] = True
                        # dbUtils doesn't have access to the grid so we can't check the path to the file...which is fine since we already checked it
                        is_Ok = insertFile(db, grid_file, run_number, args.type, args.location, check_path=False)
                        results[str(run_number)][fName]['db_status'] = is_Ok

    except Exception:
        logger.exception("Something bad happened...")

    finally:
        summary = {}
        for run, files in results.items():
            if isinstance(files, six.string_types):
                # In this case files actually contains the errMsg
                summary[run] = 'Not OK: %s' % files
                continue
            else:
                summary[run] = 'OK'
            for file, keys in files.items():
                for k, v in keys.items():
                    if 'status' in k:
                        if not v:
                            summary[
                                run] = 'Failed to insert run into db' if 'db_' in k else 'Failed to upload file to the grid'
                    if 'grid_failed' in k:
                        summary[run] = v

        logger.info("\n\n --- Summary: \n")
        logger.debug(json.dumps(results, indent=2))
        logger.info(json.dumps(summary, indent=2))
        logger.info("Summary is saved to ./uploadResults-" + time.strftime("%Y%m%d_%H%M%S"))
        with open("./uploadResults-" + time.strftime("%Y%m%d_%H%M%S"), 'w') as out_file:
            json.dump(summary, out_file, indent=2)
            json.dump(results, out_file, indent=2)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
