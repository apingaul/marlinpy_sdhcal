#!/usr/bin/env python
"""
    Configuration module for Marlin processor
"""

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import str

import os
import sys
import time
import subprocess
import argparse
import platform

# sys.path.append('/eos/user/a/apingaul/CALICE/Software/marlinpy')  #  Needed only for debugging marlinpy
from marlinpy import Marlin
from .utils import helper
from .utils.helper import InterceptHandler

import logging
if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
    logger.warning("Using python3+, you can't submit files to the grid since DIRAC API only compatible with py2")
    useLoguru = True
else:
    logger = logging.getLogger('marlinpy_sdhcal')
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    log_level = os.getenv('LOGURU_LEVEL')  # For simplicity and consistency with loguru
    logger.setLevel(log_level) if log_level is not None else logger.setLevel(logging.INFO)
    useLoguru = False
try:
    from marlinpy_sdhcal.utils import gridUtils as gdu
except ImportError:
    logger.warning(
        "Could not import Dirac, check your PYTHONPATH or source dirac env file if you plan on running on the grid")


# -----------------------------------------------------------------------------
def canRun(runNumber, runParameters, skippedRunDict, isOnGrid=False, gridUserPath=None):
    logger.info("Looking for files to process for run '%s'... " % str(runNumber))
    canContinue = True
    if not runParameters['inputFiles']:
        skippedRunDict.setdefault(runNumber, []).append("No input file found")
        logger.warning("No input file found for run '%s'" % str(runNumber))
        canContinue = False

    for inFile in runParameters['inputFiles']:
        fileFound, errMsg = helper.checkFileExists(inFile, isOnGrid, gridUserPath)
        if errMsg:
            canContinue = False
            skippedRunDict.setdefault(runNumber, []).append(errMsg)
        elif not fileFound:
            canContinue = False
            logger.warning("input file '%s' not found" % inFile)
            skippedRunDict.setdefault(runNumber, []).append("Input file '{}' not found".format(inFile))

    for outFile in runParameters['outputFiles']:
        fileFound, errMsg = helper.checkFileExists(outFile, isOnGrid, gridUserPath)
        if errMsg:
            canContinue = False
            skippedRunDict.setdefault(runNumber, []).append(errMsg)
        elif fileFound:
            canContinue = False
            logger.warning("OutputFile '%s' already present : Delete or move it before running again." % outFile)
            skippedRunDict.setdefault(runNumber, []).append("Output file '%s' already exists" % outFile)

    return canContinue


# -----------------------------------------------------------------------------
def check_section(config_section, req_keys):
    '''Check the required keys are present in the config section
    '''
    missing_keys = set(req_keys) - set(config_section.keys())
    if missing_keys:
        logger.error("Missing keys: %s" % missing_keys)
        return False
    else:
        return True


# -----------------------------------------------------------------------------
def check_config(config_file):
    '''Check all the required option are present in the config file
    '''
    is_valid = True
    # Check main sections are present
    conf = config_file.conf_dict
    if not check_section(conf, ['global', 'grid']):
        return False

    # Check global section
    logger.info("Checking section '%s'" % 'global')
    glob_keys = [
        'runOnGrid', 'xmlFile', 'marlinLibs', 'initILCSoftScript', 'processorType', 'processorPath', 'logToFile'
    ]
    if not check_section(conf['global'], glob_keys):
        is_valid = False

    if conf['global']['runOnGrid'] is True:
        logger.info("Checking section '%s'" % 'grid')
        grid_keys = ['logLevel', 'storageElement', 'outputSandbox', 'arxivName', 'ilcsoftVersion']
        if not check_section(conf['grid'], grid_keys):
            is_valid = False

    # Check run_parameters
    run_dict = config_file.run_dict
    logger.info("Checking run_parameters")
    run_keys = ['outputPath', 'outputFiles', 'inputFiles', 'marlin', 'marlinpyCfgFile']

    if conf['global']['logToFile'] is True:
        run_keys.append('logFile')
    if conf['global']['runOnGrid'] is True:
        run_keys.append('inputSandbox')
    for run, run_param in run_dict.items():
        if not check_section(run_param, run_keys):
            logger.error("From run '%s'" % run)
            is_valid = False
    return is_valid


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def main(args=None):
    '''
    '''
    parser = argparse.ArgumentParser(description='Python interface to marlin', prog='marlinpy_sdhcal')
    parser.add_argument('configFile', help='python config file')
    args = parser.parse_args()

    configFile = args.configFile
    conf = helper.importConfig(configFile)

    if not check_config(conf):
        return

    marlinDict = conf.conf_dict
    runDict = conf.run_dict
    logger.info("Running with configuration file '%s' on runs '%s'" % (configFile, str(conf.runList)))

    # Dict to hold lsit of run that could not be processed and the reason why
    skippedRunDict = {}
    arxivExists = False  # Used for grid submission, so we only package the proc libs once and not for each run

    # Grid prefix for relative path, (format is /calice/users/i/initial/) if not set assume given grid paths are absolute
    if not marlinDict['grid']['userPath']:
        marlinDict['grid']['userPath'] = None
    for run, runParameters in runDict.items():
        if not canRun(int(run),
                      runParameters,
                      skippedRunDict,
                      isOnGrid=marlinDict['global']['runOnGrid'],
                      gridUserPath=marlinDict['grid']['userPath']):
            continue

        marlin = Marlin(xmlConfig=marlinDict['global']['xmlFile'],
                        runOnGrid=marlinDict['global']['runOnGrid'],
                        libraries=marlinDict['global']['marlinLibs'],
                        ilcSoftInitScript=marlinDict['global']['initILCSoftScript'],
                        outputPath=runParameters['outputPath'],
                        outputFiles=runParameters['outputFiles'],
                        cliOptions=runParameters['marlin'])

        # Running locally
        if marlinDict['global']['runOnGrid'] is False:
            logger.info("output files : '%s'" % runParameters['outputFiles'])
            logger.info("MARLIN_DLL: '%s'" % marlinDict['global']['marlinLibs'])

            # Generate marlinpy config file
            marlin.writeConfigFile(runParameters['marlinpyCfgFile'])

            logger.info("========================")
            logger.info('Running Marlin...')
            beginTime = time.time()

            # Add a logger to save output from marlin
            if useLoguru:
                log_level = os.getenv('LOGURU_LEVEL')
                logging.getLogger('marlinpy').handlers.clear()
                logging.getLogger('marlinpy').addHandler(InterceptHandler())
                logging.getLogger('marlinpy').setLevel(log_level if log_level is not None else 10)

            else:
                marlinLog = logging.getLogger('marlinpy')
                marlinLog.setLevel(logging.DEBUG)
                formatter = logging.Formatter(
                    '%(asctime)s : [%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s')

            if marlinDict['global']['logToFile'] is True:
                if useLoguru:
                    # logger.remove()
                    # logger.add(sys.stderr, filter='marlinpy_sdhcal')
                    logger.add(runParameters['logFile'], enqueue=True, filter='marlinpy.marlinpy')
                else:
                    fH = logging.FileHandler(runParameters['logFile'], mode='w')
                    fH.setFormatter(formatter)
                    fH.setLevel(logging.DEBUG)
                    marlinLog.addHandler(fH)

                logger.warning("Processor output is logged to '%s'" % runParameters['logFile'])

            marlin.run(runParameters['marlinpyCfgFile'])

            logger.info('Finished running Marlin')

            # TODO: add files location into db and change the valid flag to true if all was ok
            # dirac-repo-create-lfn-list -r ../Streamout/Streamout.rep
            try:
                helper.elapsedTime(beginTime)
            except ValueError:
                logger.error("Can't print time...")
            logger.info("========================\n")
            if not useLoguru:
                logger.removeHandler(fH)

        else:
            # TODO: add check on mandatory keys
            input_files = [os.path.basename(in_file) for in_file in runParameters['inputFiles']]
            output_files = [os.path.basename(out_file) for out_file in runParameters['outputFiles']]
            logger.debug("marlinCliOptions: %s " % marlin.cliOptions)
            extra_cli_args = marlin.getFormattedCliOptions()
            xml_file = os.path.basename(marlinDict['global']['xmlFile'])
            libs = [os.path.basename(lib) for lib in marlinDict['global']['marlinLibs']]
            log_file = os.path.basename(runParameters['logFile'])
            logger.debug("InputFiles (on grid) : %s" % runParameters['inputFiles'])
            logger.debug("InputFiles (on node): %s" % input_files)
            logger.debug("cliArgs : %s" % extra_cli_args)
            logger.debug("xml_file : %s" % xml_file)
            logger.debug("libs : %s" % libs)
            logger.debug("log_file : %s" % log_file)

            if not arxivExists:
                gdu.makeArxiv(folderToArxiv=marlinDict['global']['processorPath'] + '/lib',
                              arxivName=marlinDict['grid']['arxivName'])
                arxivExists = True

            if not runParameters['repoLocation']:
                repoLocation = marlinDict['global']['processorType']
            else:
                repoLocation = runParameters['repoLocation']

            dIlc = gdu.getDiracInstance(withRepo=True, repoLocation=repoLocation.replace('.rep', '') +
                                        '.rep')  # Ensure it ends with .rep

            marlin_dirac = gdu.configureDiracMarlin(steering_file=xml_file,
                                                    processor_lib_list=libs,
                                                    input_file=input_files,
                                                    log_file=log_file,
                                                    extra_cli_args=extra_cli_args,
                                                    ilcsoft_version=marlinDict['grid']['ilcsoftVersion'])

            job = gdu.createJob(application=marlin_dirac,
                                lfns=output_files,
                                input_data=runParameters['inputFiles'],
                                output_path=runParameters['outputPath'],
                                name=marlinDict['global']['processorType'] + '_' + str(run),
                                group=marlinDict['global']['processorType'],
                                log_level=marlinDict['grid']['logLevel'],
                                SE=marlinDict['grid']['storageElement'],
                                output_sandbox=marlinDict['grid']['outputSandbox'],
                                input_sandbox=runParameters['inputSandbox'])

            logger.info(" --- Submitting Job ... ")
            logger.debug(job._toJDL())
            # with open(marlinDict['global']['processorType'] + '_' + str(run) + ".jdl", 'w') as out_file:
            # out_file.write(job._toJDL())
            job.submit(dIlc, mode=marlinDict['grid']['backend'])
            logger.info(" --- Submitting Job Done ")
            logger.info(" --- Job info stored in '%s' " % repoLocation)

            # TODO: add files location into db and change the valid flag to true if all was ok
            # dirac-repo-create-lfn-list -r ../Streamout/Streamout.rep

    if skippedRunDict:
        logger.info('-' * 120)
        logger.info("--- Following runs were not analysed: ")
        for run, reason in skippedRunDict.items():
            logger.info("\t\t %s : %s" % (str(run), reason))
        logger.info('-' * 120)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
