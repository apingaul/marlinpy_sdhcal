#!/usr/bin/env python
'''
'''
# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
import six

import os
import argparse
import platform
import json
import time
import subprocess

from marlinpy_sdhcal.utils import helper
from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)

from marlinpy_sdhcal.utils import gridUtils as gdu


# -----------------------------------------------------------------------------
def insertFile(db, file, run_number, file_type, location_type, check_path=True):
    ''' Returns True/False
    '''
    files_indb_list = db.getFilesFromRun(run_number=run_number, file_type=file_type, location_type=location_type)
    if files_indb_list:
        for f in files_indb_list:
            if f == file:
                logger.warning("File '%s' already in db, not adding it" % f)
                continue
        logger.warning("There are already '%s' files associated with run '%s' in the db: " % (file_type, run_number))
        logger.warning(files_indb_list)

    ans = db.insertRunFile(run_number=run_number,
                           path_to_file=file,
                           file_type=file_type,
                           location_type=location_type,
                           check_path=check_path,
                           commit=True)
    if ans is not None and ans != 1:  # insertRunFile returns the number of file uploaded to the db, should be 1 or None in this case
        logger.error("Weird number of db entry modified: got '%s', expected 1" % ans)
        return False
    return True
    # -----------------------------------------------------------------------------


def downloadFile(grid_path, local_path):
    results = {"dl_status": False, "grid_path": grid_path, "local_path": local_path}

    file_found, errMsg = gdu.findFileOnGrid(grid_path)
    if errMsg:
        # Error happened when checking file on the grid
        logger.error('Something bad happened: %s' % errMsg)
        return results
    elif not file_found:
        logger.error("Could not find file '%s' on the grid" % grid_path)
        return results

    cmd = 'source /cvmfs/clicdp.cern.ch/DIRAC/bashrc;'
    cmd += "dirac-dms-get-file {grid_file}".format(grid_file=grid_path)
    logger.debug("download command: '%s'" % cmd)
    try:
        proc = subprocess.Popen(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                                shell=True,
                                universal_newlines=True,
                                cwd=os.path.dirname(local_path))
        logger.debug("\n\n Answer: ")
        output, _ = proc.communicate()
        for line in output.split('\n'):
            logger.info(line)
        # Dirac output a dict with keys enclosed with '' instead of ""
        dirac_ans = json.loads(output.replace("'", '"'))
        if dirac_ans['Successful'][grid_path] != local_path:
            logger.error("Failed to download file '%s' to '%s' with cmd '%s' " % (grid_path, local_path, cmd))
            logger.error("Dirac full answer: '%s'" % dirac_ans)
            return results
    except subprocess.CalledProcessError:
        logger.error("Failed to download file '%s' to '%s' with cmd '%s' " % (grid_path, local_path, cmd))
    results['dl_status'] = True
    return results


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def main(args=None):
    parser = argparse.ArgumentParser(
        description=
        'Dl file from grid to local folder. Pass --runList 0 if you want to process every runs from every testbeams. Pass --registerInDb to insert files into the db (you also need to pass --db_conf --location and --type in this case)',
        prog=__file__)

    parser.add_argument(
        '--format',
        help=
        'File format (on (Dirac) grid), requires {tb_name} and {run_number}! eg. /eos/project/s/sdhcal/data/{tb_name}/Raw/DHCAL_{run_number}_I0_0.slcio. Use with --runList and --db_conf to generate file name. Use --fullPath if you want to download specific files'
    )

    parser.add_argument(
        '--fullPath',
        help='Full file path (on (Dirac) grid. Cannot use register on db, use --format and --runList for this.')

    parser.add_argument(
        '--localFolder',
        required=True,
        help='Path to local folder where to download file. You can use {tb_name} in the path IF you also use --format.')

    parser.add_argument('--registerInDb',
                        action='store_true',
                        default=False,
                        help='Whether to store the downloaded file to the db. ')

    parser.add_argument('--db_conf', help='Path to json file with db parameters')

    parser.add_argument(
        '--location',
        help=
        'Where the file is stored (eos_sdhcal, local, grid, etc.) Corresponds to an entry in `files_locations_types` table in the db'
    )

    parser.add_argument(
        '--type',
        help='Type of the file (raw, trivent, etc.) Corresponds to an entry in `analysis_versions` table in the db')

    parser.add_argument(
        '--runList',
        nargs='+',
        type=int,
        help=
        'List of run number separated by a space. To process every run with the given format use --runList 0. Note that only runs present in the db will be processed!'
    )
    args = parser.parse_args()

    if args.fullPath is not None:
        fName = os.path.basename(args.fullPath)
        local_path = '/'.join([args.localFolder, fName])
        logger.info(downloadFile(args.fullPath, local_path))
        return

    elif args.format is None:
        parser.error("You need to use --format or fullPath to donload files")

    if args.format is not None and (args.db_conf is None or args.runList is None):
        parser.error("--format requires you set --db_conf, and --runList")

    if args.registerInDb and (args.db_conf is None or args.type is None or args.location is None):
        parser.error("--registerInDb requires you set --db_conf, --type and --location ")

    if args.db_conf:
        db = dbu(db_conf=args.db_conf)

    if args.runList[0] == 0:
        testbeams = db.executeCmd('SELECT `idx`,`name` FROM `testbeams`')

    else:
        testbeams = []
        tbIdxSet = set()
        for run in args.runList:
            tb_idx = db.getTestBeamIdxFromRun(run)
            if not tb_idx in tbIdxSet:
                tbIdxSet.add(tb_idx)
                testbeams.append({"idx": tb_idx, 'name': db.getTestBeamNameFromRun(run)})
    ''' results will hold list of files the exec tried to update and the status
        grid_* are only added if trying to upload to the grid
        db_status = True means the file is in the db
        grid_status = True means the file was uploaded to the grid
        If upload fails because the file is already present, then grid_status will be set to False but db_status can still be True

        eg = {
            "730677": {
                "DHCAL_730677_I0_0.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_0.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_0.slcio"
                },
                "DHCAL_730677_I0_1.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_1.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_1.slcio"
                },
                "DHCAL_730677_I0_2.slcio": {
                    "db_status": True,
                    "grid_status": True,
                    "local_path": "/eos/project/s/sdhcal/data/SPS_10_2015/Raw/DHCAL_730677_I0_2.slcio",
                    "grid_path": "/calice/users/a/apingaul/data/SPS_10_2015/Raw/DHCAL_730677_I0_2.slcio"
                },
            }
        }
    '''

    results = {}
    path = args.format
    logger.info('Downloading files for run: %s' % args.runList)
    try:
        for tb in testbeams:
            tb_idx = tb['idx']
            tb_name = tb['name']
            runList = db.getRunList({'tb_idx': tb_idx})
            if args.runList[0] != 0:
                # keep only runs from args.runList AND present in this tb
                runList = list(set(runList) & set(args.runList))

            if not runList:
                logger.error("Could not generate runList for tb '%s', check your parameters" % tb_name)
                continue

            logger.info("Downloading files for tb '%s' run: %s" % (tb, args.runList))
            for run_number in runList:
                results[str(run_number)] = {}

                try:
                    file_format = path.format(tb_name=tb_name, run_number=run_number)
                    dir_name = args.localFolder.replace("{tb_name}", tb_name)
                    file_list = helper.findAllInputFiles([file_format], run_number, isOnGrid=True)
                except OSError as e:
                    logger.error(e)
                    results[str(run_number)] = "Exception occurred when looking for files"
                    continue

                if not file_list:
                    errMsg = "No files found for run '%s' with type %s" % (run_number, args.type)
                    results[str(run_number)] = errMsg
                    logger.error(errMsg)
                    continue

                logger.debug("List of files to download for run " + str(run_number) + ': ' + str(file_list))
                for f in file_list:
                    fName = os.path.basename(f)
                    local_path = '/'.join([dir_name, fName])
                    results[str(run_number)][fName] = downloadFile(f, local_path)

                    if args.registerInDb:
                        is_Ok = insertFile(db, local_path, run_number, args.type, args.location, check_path=True)
                        results[str(run_number)][fName]['db_status'] = is_Ok
    except Exception as e:
        logger.exception("Something bad happened...: %s" % e)

    finally:
        summary = {}
        for run, files in results.items():
            if isinstance(files, six.string_types):
                # In this case files actually contains the errMsg
                summary[run] = 'Not OK: %s' % files
                continue
            else:
                summary[run] = 'OK'
            for file, keys in files.items():
                for k, v in keys.items():
                    if 'status' in k:
                        if not v:
                            summary[
                                run] = 'Failed to insert run into db' if 'db_' in k else 'Failed to download file from the grid'

        logger.info("\n\n --- Summary: \n")
        logger.debug(json.dumps(results, indent=2))
        logger.info(json.dumps(summary, indent=2))
        logger.info("Summary is saved to ./uploadResults-" + time.strftime("%Y%m%d_%H%M%S"))
        with open("./downloadResults-" + time.strftime("%Y%m%d_%H%M%S"), 'w') as out_file:
            json.dump(summary, out_file, indent=2)
            json.dump(results, out_file, indent=2)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
