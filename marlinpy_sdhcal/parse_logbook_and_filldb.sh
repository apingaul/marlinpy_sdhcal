declare -a testbeams=("SPS_08_2012"
    "SPS_11_2012"
    "SPS_12_2014"
    "SPS_04_2015"
    "PS_06_2015"
    "SPS_10_2015"
    "SPS_06_2016"
    "SPS_10_2016"
    "SPS_09_2018"
    "SPS_09_2017"
)
root_folder="$(pwd)/../../"
db_conf=$1
export LOGURU_LEVEL=INFO
for tb in "${testbeams[@]}"; do
    echo $tb
    xml=${root_folder}/data/ElogData/logbook_${tb}.xml
    json=${root_folder}/data/ElogData/logbook_${tb}.json
    parse_logbook ${xml} --outputJsonLogBook ${json}
    insert_logbook_into_database ${json} --db_conf ${db_conf}
done
unset LOGURU_LEVEL
