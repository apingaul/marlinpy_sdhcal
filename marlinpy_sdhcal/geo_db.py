#!/usr/bin/env python
''' Small utility to interact (insert, update, remove to/fro json file) with geometry db
    Requires python3! comment out the lines with logger if you really need to run with python2
'''
# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os
import sys
import argparse
import platform
from lxml import etree
import json

from marlinpy_sdhcal.utils import helper
from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def main(args=None):
    parser = argparse.ArgumentParser(description='Small utility to insert json geometry into db', prog=__file__)

    # Required
    group_ex = parser.add_mutually_exclusive_group(required=True)
    group_ex.add_argument("--remove", action='store_true', help="remove geometry associated with testbeam from db")
    group_ex.add_argument("--add", action='store_true', help="Add geometry associated with testbeam from db")
    group_ex.add_argument("--update", action='store_true', help="Update geometry associated with testbeam from db")
    group_ex.add_argument("--dump",
                          action='store_true',
                          help="Dump geometry associated with testbeam from db to jsonFile")

    parser.add_argument('--testbeam', required=True, help='testbeam to associate the geometry with')
    parser.add_argument('--db_conf', required=True)

    # Optional
    parser.add_argument('--jsonGeometry', help='json file with geometry')
    parser.add_argument('--name', help='geometry name in db. Will use file name from --jsonGeometry if not set')

    args = parser.parse_args()

    if ('add' in vars(args) or 'update' in vars(args) or 'dump' in vars(args)) and args.jsonGeometry is None:
        parser.error('The jsonGeometry argument is required to insert/update the db or dump from it')

    fName, fExt = os.path.splitext(os.path.basename(args.jsonGeometry))
    if fExt != '.json':
        raise IOError(logger.exception("Geometry file should be in a json format, found '{}'".format(fExt)))

    geoName = args.name if args.name else fName
    db = dbu(db_conf=args.db_conf)

    if args.add is True:
        helper.insertNewGeometryIntoDb(db, args.jsonGeometry, args.testbeam, geoName)
    elif args.update is True:
        helper.updateGeometryInDb(db, args.jsonGeometry, args.testbeam, geoName)
    elif args.remove is True:
        helper.removeGeometryFromDb(db, args.testbeam, geoName)
    if args.dump is True:
        helper.generateGeometryFileFromDb(db=db,
                                          tbName=args.testbeam,
                                          geoName=geoName,
                                          filePath=os.path.dirname(args.jsonGeometry))
    logger.info("Success!")


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
