#!/usr/bin/env python

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os
import json
import argparse
import platform

from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)


# do validation and checks before insert
def validate_string(val):
    if val != None:
        if type(val) is int:
            #for x in val:
            #   print(x)
            return str(val).encode('utf-8')
        else:
            return val


def main(args=None):
    parser = argparse.ArgumentParser(description='', prog='fillDbFromJson')
    parser.add_argument('logbook', help='parsed logbook with json format')
    parser.add_argument('--db_conf', required=True)
    args = parser.parse_args()

    logBook = json.load(open(args.logbook))

    # print(json.dumps(run, indent=2))
    db = dbu(db_conf=args.db_conf)

    for run in logBook:
        # if run['state'] is not None:
        logger.debug('run: {run} - date: {date}'.format(run=run, date=run['runDate']))

        # logger.debug(run['runNumber'], run['state'], run['state'], run['runDate'], run['runEnergy'], run['runBeam'],
        #       0 if run['dataOk'] is False else 1)
        cmd = "SELECT `run` FROM `logbook` WHERE (`run`=%s)" % run['runNumber']
        already_exist = db.executeCmd(cmd, fetch_all=False)
        if not already_exist:
            tb_idx = '(SELECT `idx` FROM `testbeams` WHERE (%(date)s BETWEEN `debut` AND `fin`))'
            geo_idx = '(SELECT `idx` FROM `geometry_versions` WHERE (`tb_idx`={tb_idx}))'.format(tb_idx=tb_idx)
            # args = {'date': run['runDate']}
            cmd = "INSERT INTO `logbook` (`mid`, `run`, `daq`, `configuration`, `date`, `energy`, `beam`, `thresh0`, `thresh1`, `n_evt`, `absorb`, `hv`, `gas`, `pos`, `tb_idx`, `geo_idx`, `comment`, `valid`) VALUES (%(elog_id)s, %(run_id)s, %(daq)s, %(config)s , %(date)s, %(energy)s, %(beam)s, %(thresh0)s, %(thresh1)s, %(n_evt)s, %(absorb)s, %(hv)s, %(gas)s, %(pos)s, {tb_idx}, {geo_idx}, %(comment)s, %(valid)s)".format(
                tb_idx=tb_idx, geo_idx=geo_idx)
            args = {
                'elog_id': run['elog_id'],
                'run_id': run['runNumber'],
                'daq': run['state'],
                'config': run['state'],
                'date': run['runDate'],
                'energy': run['runEnergy'],
                'beam': run['runBeam'],
                'thresh0': run['thresh0'],
                'thresh1': run['thresh1'],
                'n_evt': run['nEvts'],
                'absorb': run['absorb'],
                'hv': run['hvValue'],
                'gas': run['gas'],
                'pos': run['Pos'],
                'comment': run['Comments'],
                'valid': int(run['dataOk']) if run['dataOk'] is not None else None
            }

            db.executeCmd(cmd=cmd, args=args, is_insert=True)
        else:
            logger.error('Run {} already exists'.format(run["runNumber"]))


if __name__ == "__main__":
    main()
