#!/usr/bin/env python
'''
    Utils to to get informations from GEOMETRY db
    After installing mysql server
    sudo mysqld_safe
    mysql -u root
    mysql> CREATE DATABASE GEOMETRY;
    mysql> USE GEOMETRY;
    mysql> CREATE USER 'acqilc'@'localhost'
    mysql> GRANT ALL PRIVILEGES ON GEOMETRY.* TO 'acqilc'@'localhost';
    # ON lyosdhcal10
    mysqldump -u acqilc -pRPC_2008  GEOMETRY > geometry_m3_2017.sql
    #locally
    mysqldump -u acqilc -pRPC_2008  GEOMETRY < geometry_m3_2017.sql
    if not working:
    mysql -u acqilc -pRPC_2008
    use GEOMETRY;
    source geometry_m3_2017;
'''

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import str

import os
import sys
import platform
import pymysql.cursors
import subprocess
import json
from datetime import datetime

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__name__)


class DbUtils(object):
    def __init__(self, host='', user='', passwd='', db='', unix_socket='', port='', ssl=None, db_conf=None):
        if db_conf:
            db_json = json.load(open(db_conf))
            #Check for keys?
            self.__init__(host=db_json['host'],
                          user=db_json['user'],
                          passwd=db_json['passwd'],
                          db=db_json['name'],
                          port=db_json['port'],
                          ssl=db_json['ssl'],
                          unix_socket=db_json['socket'])
        else:
            self.host = host
            self.unix_socket = unix_socket
            self.user = user
            self.passwd = passwd
            self.dbName = db
            self.port = int(port)
            self.ssl = ssl
            if not self.unix_socket:
                self.db = pymysql.connect(host=self.host,
                                          user=self.user,
                                          passwd=self.passwd,
                                          db=self.dbName,
                                          port=self.port,
                                          ssl=self.ssl,
                                          cursorclass=pymysql.cursors.DictCursor)
            else:
                self.db = pymysql.connect(unix_socket=self.unix_socket,
                                          user=self.user,
                                          passwd=self.passwd,
                                          db=self.dbName,
                                          ssl=self.ssl,
                                          cursorclass=pymysql.cursors.DictCursor)

    # -----------------------------------------------------------------------------
    def executeCmd(self, cmd, args=None, is_insert=False, fetch_all=True):
        ''' `cmd` the query to execute, `args` (tuple, list or dict) - parameters used with query. (optional)
            If args is a list or tuple, %s can be used as a placeholder in the query. If args is a dict, %(name)s can be used as a placeholder in the query.
            `fetch_all` = False : Get only the first result from the query, all results otherwise
            `is_insert`: If the query should modify the db
            Return:
                - number of affected rows if is_insert=True
                - List of rows if fetch_all=True (a row is a dict with keys corresponding to db columns)
                - Dict (a single row) if fetch_all=False
        '''
        with self.db.cursor() as cursor:
            if args is not None:
                logger.debug("SQL CMD: " + cmd % args)
            else:
                logger.debug("SQL CMD: " + cmd)

            affected_rows = cursor.execute(cmd, args)
            if is_insert is False:
                if fetch_all is True:
                    return cursor.fetchall()
                else:
                    return cursor.fetchone()
        # insert command, need to commit
        self.db.commit()
        return affected_rows

    # -----------------------------------------------------------------------------
    def getListOfTestBeamsNames(self):
        ''' Return list of test beam names registered in db
        '''
        tb_list = self.executeCmd("SELECT `name` FROM `testbeams`")
        if tb_list is not None:
            return [tb['name'] for tb in tb_list]

    # -----------------------------------------------------------------------------
    def getTestBeamIdx(self, tb_name):
        ''' Return tb_idx for selected test beam
            Return None if not found
        '''
        cmd = 'SELECT `idx` FROM `testbeams` WHERE `name`=%s'
        args = tb_name
        idx = self.executeCmd(cmd, args, fetch_all=False)

        if 0 != idx and idx is not None:
            return idx['idx']

        logger.error("Selected Test Beam '%s' not found in database..." % tb_name)
        logger.error("List of available Test Beam :")
        logger.error(self.getListOfTestBeamsNames())

    # -----------------------------------------------------------------------------
    def getTestBeamIdxFromGeometry(self, geo_idx):
        ''' Return tb_idx associated with geo_idx
        '''
        cmd = 'SELECT `tb_idx` FROM `geometry_versions` WHERE `idx`=%s'
        args = geo_idx
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['tb_idx']
        logger.error("Could not find TestBeam associated with geometry '%s' " % self.getGeometryName(geo_idx))

    # -----------------------------------------------------------------------------
    def getTestBeamNameFromIdx(self, tb_idx):
        ''' Return tb name associated with tb_idx
            Return None if not found
        '''
        cmd = 'SELECT `name` FROM `testbeams` WHERE (`idx`=%s)'
        args = tb_idx
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['name']
        logger.error("Could not find TestBeam associated with index '%s' " % tb_idx)

    # -----------------------------------------------------------------------------
    def getTestBeamNameFromRun(self, run_number):
        ''' Return tb name associated with run_number
            Return None if not found
        '''
        tb_idx = self.executeCmd('SELECT `tb_idx` FROM `logbook` WHERE (`run`=%s)', run_number, fetch_all=False)
        if tb_idx is not None:
            return self.getTestBeamNameFromIdx(tb_idx['tb_idx'])
        logger.error("Could not find TestBeam associated with run '%s' " % run_number)

    # -----------------------------------------------------------------------------
    def getListOfGeometryNames(self, tb_idx):
        ''' Return list of geometry names registered in db for given tb_idx
            Return None if nothing found
        '''
        cmd = 'SELECT `name` FROM `geometry_versions` WHERE (`tb_idx`=%s)'
        args = tb_idx
        geoList = []
        for row in self.executeCmd(cmd, args):
            for _, name in row.items():
                geoList.append(name)

        return geoList

    # -----------------------------------------------------------------------------
    def getGeometryIdx(self, geo_name, tb_idx, silent=False):
        ''' Return Idx in db for selected geometry
            Return None if not found
        '''
        cmd = 'SELECT `idx` FROM `geometry_versions` WHERE (`name` = %(geo_name)s AND `tb_idx` = %(tb_idx)s)'
        args = {"geo_name": geo_name, "tb_idx": tb_idx}
        idx = self.executeCmd(cmd, args, fetch_all=False)

        if idx is not None:
            return idx['idx']

        args['geo_name'] = geo_name.lower()
        if silent is False:
            tb_name = self.getTestBeamNameFromIdx(tb_idx)
            geo_list = self.getListOfGeometryNames(tb_idx)
            not_found = True
            for geo in geo_list:
                if geo.lower() == geo_name.lower():
                    logger.warning("Found geometry %s with a different capitalisation")
                    not_found = False

            if not_found:
                logger.error("Selected Geometry '%s' for test beam '%s' not found in database..." % (geo_name, tb_name))
                logger.error("List of available Geometry for this test beam :")
                logger.error(geo_list)

    # -----------------------------------------------------------------------------
    def getGeometryIdxFromRun(self, run_number):
        ''' Returns geo_idx associated with given run_number
            Return None if geo_idx notFound
        '''
        cmd = 'SELECT `geo_idx` FROM `logbook` WHERE (`run`= %s)'
        args = run_number
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['geo_idx']
        logger.error("Could not find geometry associated with run '%s' " % run_number)

    # -----------------------------------------------------------------------------
    def getGeometryName(self, geo_idx):
        ''' Return geo_name associated with geo_idx
            Return None if not found
        '''
        cmd = 'SELECT `name` FROM `geometry_versions` WHERE (`idx`=%s)'
        args = geo_idx
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['name']
        logger.error("Could not find geometry associated with index '%s' " % self.getGeometryName(geo_idx))

    # -----------------------------------------------------------------------------
    def getTestBeamIdxFromRun(self, run_number):
        ''' Return tb_idx associated with given run_number
            Return None if not found
        '''
        cmd = 'SELECT `idx` FROM `testbeams` WHERE ((SELECT DATE(`date`) FROM `logbook` WHERE (`run`=%s)) BETWEEN DATE(`debut`) AND DATE(`fin`))'
        args = run_number
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['idx']
        logger.error("Could not find TestBeam associated with run '%s' " % run_number)

    # -----------------------------------------------------------------------------
    def getRowsOfSlot(self, geo_idx):
        ''' Return list rows from table `chambers` associated with geometry
            Return Format is list of dict with keys {slot_id, x0, y0, z0, x1, y1, z1}
            slot_id starts at 0
            Return None if nothing found
        '''
        cmd = 'SELECT `num` as `slot_id`, `x0`, `y0`, `z0`, `x1`, `y1`, `z1` FROM `chambers` WHERE (`geometry_idx`=%s)'
        args = geo_idx
        return self.executeCmd(cmd, args)

    # -----------------------------------------------------------------------------
    def getListOfSlot(self, geo_idx):
        ''' Return list of slots  associated with geometry
            Slots starts at 0
            Return None if nothing found
        '''
        cmd = 'SELECT `num` FROM `chambers` WHERE (`geometry_idx` = %s)'
        args = geo_idx
        slotList = self.executeCmd(cmd, args)
        if slotList is not None:
            return list(slot['num'] for slot in slotList)
        logger.error("No slots associated with geometry '%s' and tb '%s' " % self.getGeometryName(geo_idx),
                     self.getTestBeamNameFromIdx(self.getTestBeamIdxFromGeometry(geo_idx)))

    # -----------------------------------------------------------------------------
    def getRowsOfDif(self, geo_idx):
        '''Return list of rows from table dif associated  with given geometry
           Return format is a list of dict with keys {dif_id, slot_id, posI, posJ, shiftI, shiftJ}
           slot_id starts at 0
        '''
        slot_id = "(SELECT `num` FROM `chambers` WHERE (`idx` = `difs`.`chamber_idx`))"

        cmd = 'SELECT `num` AS `dif_id`, %(slot_id)s AS `slot_id`, `dI` AS `posI`, `dJ` AS `posJ`, `polI` AS `shiftI`, `polJ` AS `shiftJ` FROM `difs` WHERE (%(slot_id)s IS NOT NULL AND `geometry_idx` = %(geo_idx)s)'
        args = {"slot_id": slot_id, "geo_idx": geo_idx}
        return self.executeCmd(cmd, args)

    # -----------------------------------------------------------------------------
    def getListOfDif(self, geo_idx, is_valid=True):
        ''' Return list of difs  associated with geometry
            By default only get difs that are valid, call this a second time with is_valid=False to also get the difs to skips
            Return None if not found
        '''
        cmd = 'SELECT `num` FROM `difs` WHERE (`geometry_idx`=%(geo_idx)s AND `valid`=%(is_valid)s)'
        args = {"geo_idx": geo_idx, "is_valid": int(is_valid)}
        difList = self.executeCmd(cmd, args)
        if difList is not None:
            return list(dif['num'] for dif in difList)
        logger.error("Could not find difs associated with geometry '%s' " % self.getGeometryName(geo_idx))

    # -----------------------------------------------------------------------------
    def getChamberIdx(self, slot_id, geo_idx):
        ''' Return idx of slot_id in associated geometry
            Return None if not found
        '''
        cmd = 'SELECT `idx` FROM `chambers` WHERE (`num`=%(slot_id)s AND `geometry_idx`=%(geo_idx)s)'
        args = {"slot_id": slot_id, "geo_idx": geo_idx}
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['idx']
        logger.error("Could not find slot '%s' associated with geometry '%s' " %
                     (slot_id, self.getGeometryName(geo_idx)))

    # -----------------------------------------------------------------------------
    def checkDbGeometry(self, slotList):
        pass

    # -----------------------------------------------------------------------------
    # -----------------------------------------------------------------------------
    def updateGeometry(self, slotList, tb_name, geo_name, difToSkipList=None, slot_width=2.8):
        '''
            Expect a full geometry in slotList, if the number of slots in the new geometry != than what is stored in db,
            it will delete the slots that are not present in the json file from the db (or add the new ones).
            raise ValueError if tb_name or geo_name not found
            TODO: Test
            TODO: Add table with dif to skip + reason
        '''
        if difToSkipList is None:
            difToSkipList = []

        tb_idx = self.getTestBeamIdx(tb_name)
        if tb_idx is None:
            errMsg = "TB '%s' doesn't exist" % tb_name
            logger.error(errMsg)
            raise ValueError(errMsg)

        geo_idx = self.getGeometryIdx(geo_name, tb_idx)
        if geo_idx is None:
            errMsg = "Geometry '%s' doesn't exist for testBeam '%s'" % (geo_name, tb_name)
            logger.error(errMsg)
            raise ValueError(errMsg)

        # Check there is same number of slot in both geometries
        # Dif check is done at the end for simplicity
        dbSlotList = self.getListOfSlot(geo_idx)
        dbDifList = self.getListOfDif(geo_idx)
        dbSkipDifList = self.getListOfDif(geo_idx, is_valid=False)

        newSlots = list()
        removeSlots = list()

        if len(dbSlotList) != len(slotList):
            logger.warning("New geometry doesn't have the same number of slot as stored in the db : new = %s, db = %s" %
                           (len(slotList), len(dbSlotList)))
            if len(dbSlotList) < len(slotList):  # New slots in new geometry -> will be added in the loop
                newSlots = list(set(slot['slot'] for slot in slotList) - set(dbSlotList))
                logger.warning("Will add the following slots to the db: '%s'" % (str(newSlots)))
            else:  # Missing slots in new geometry -> will ve deleted after removing ref to them in difs
                removeSlots = list(set(dbSlotList) - set(slot['slot'] for slot in slotList))
                logger.warning("Will remove the following slots from the db: '%s'" % (str(removeSlots)))

        difList = []
        for slot in slotList:
            slot_id = slot['slot']
            left = slot.get('left', None)
            center = slot.get('center', None)
            right = slot.get('right', None)
            z0 = int(slot_id) * slot_width

            self.updateSlotInDb(slot_id, z0, left, center, right, geo_idx)
            if left is not None:
                difList.append(left)
                self.updateDifInDb(dif_id=left, dif_pos=0, slot_id=slot_id, geo_idx=geo_idx)
            if center is not None:
                difList.append(center)
                self.updateDifInDb(dif_id=center, dif_pos=32, slot_id=slot_id, geo_idx=geo_idx)
            if right is not None:
                difList.append(right)
                self.updateDifInDb(dif_id=right, dif_pos=64, slot_id=slot_id, geo_idx=geo_idx)
            # TODO: Check all were properly added?

        for dif in difToSkipList:
            self.updateDifInDb(dif_id=dif, dif_pos=None, slot_id=None, geo_idx=geo_idx, is_valid=False)

        # Now check the numbers of difs, new ones have already been added in the loop, ones not present in new geometry are deleted here
        removeDifs = list()
        fullDbDifList = dbDifList + dbSkipDifList
        fullDifList = difList + difToSkipList
        newDifs = set(fullDifList) - set(fullDbDifList)
        if len(fullDbDifList) != len(fullDifList):
            if len(fullDifList) < len(fullDbDifList):
                removeDifs = list(set(fullDbDifList) - set(fullDifList))
                logger.warning("Will remove the following difs from the db: '%s'" % (str(removeDifs)))
                for dif in removeDifs:
                    self.removeDifFromDb(dif_id=dif, geo_idx=geo_idx)

        if removeSlots:
            for slot in removeSlots:
                self.removeSlotFromDb(slot_id=slot, geo_idx=geo_idx)

        logger.info("Successfully updated geometry '%s' for tb '%s': " % (geo_name, tb_name))
        logger.info("\t -- Updated/Added/Removed Slot: %s / %s / %s" %
                    (len(slotList) - len(newSlots), len(newSlots), len(removeSlots)))
        logger.info("\t -- Updated/Added/Removed Difs: %s / %s / %s" %
                    (len(difList) - len(newDifs), len(newDifs), len(removeDifs)))

    # -----------------------------------------------------------------------------
    def updateSlotInDb(self, slot_id, slot_pos, dif_left, dif_center, dif_right, geo_idx):
        ''' Update the slot in `chambers`, add it if it doesn't exist
            slot_id should start from 0
        '''
        existCmd = 'SELECT `num` FROM `chambers` WHERE (`num`=%(slot_id)s and `geometry_idx`=%(geo_idx)s)'
        args = {"slot_id": slot_id, "geo_idx": geo_idx}
        does_exist = self.executeCmd(existCmd, args, fetch_all=False)
        if does_exist is None:
            logger.warning("Slot '%s' for geometry '%s' in testbeam '%s' doesn't exist in db, adding it" %
                           (slot_id, self.getGeometryName(geo_idx),
                            self.getTestBeamNameFromIdx(self.getTestBeamIdxFromGeometry(geo_idx))))
            self.insertNewSlotIntoDb(slot_id, slot_pos, dif_left, dif_center, dif_right,
                                     self.getTestBeamIdxFromGeometry(geo_idx), geo_idx)
            return

        cmd = 'UPDATE `chambers` SET `num`=%(slot_id)s, `z0`=%(z0)s, `z1`=%(z0)s, `left`=%(left)s, `center`=%(center)s, `right`=%(right)s WHERE (`geometry_idx`=%(geo_idx)s AND `num`=%(slot_id)s)'
        args = {
            'slot_id': slot_id,
            'z0': slot_pos,
            'left': dif_left,
            'center': dif_center,
            'right': dif_right,
            'geo_idx': geo_idx
        }
        self.executeCmd(cmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def updateDifInDb(self, dif_id, dif_pos, slot_id, geo_idx, is_valid=True):
        ''' Update the dif in `difs`, add it if it doesn't exist
            slot_id should start from 0
            raise ValueError if slot_id not found in db
        '''
        existCmd = 'SELECT `num` FROM `difs` WHERE (`num`=%(dif_id)s and `geometry_idx`=%(geo_idx)s)'
        args = {"dif_id": dif_id, "geo_idx": geo_idx}
        does_exist = self.executeCmd(existCmd, args, fetch_all=False)
        if does_exist is None:
            logger.warning("Dif '%s' for geometry '%s' in testbeam '%s' doesn't exist in db, adding it" %
                           (dif_id, self.getGeometryName(geo_idx),
                            self.getTestBeamNameFromIdx(self.getTestBeamIdxFromGeometry(geo_idx))))
            self.insertNewDifIntoDb(dif_id,
                                    dif_pos,
                                    slot_id,
                                    self.getTestBeamIdxFromGeometry(geo_idx),
                                    geo_idx,
                                    is_valid=is_valid)
            return

        chamber_idx = self.getChamberIdx(slot_id=slot_id, geo_idx=geo_idx)
        if chamber_idx is None and is_valid:
            errMsg = "Can't update dif '%s' from slot '%s' associated with geometry '%s' " % (
                dif_id, slot_id, self.getGeometryName(geo_idx))
            logger.error(errMsg)
            raise ValueError(errMsg)

        difCmd = 'UPDATE `difs` SET `num`=%(dif_id)s, `dJ`=%(dif_pos)s, `chamber_idx`=%(chamber_idx)s, `valid`=%(is_valid)s WHERE (`geometry_idx`=%(geo_idx)s AND `num`=%(dif_id)s)'
        args = {
            "dif_id": dif_id,
            "dif_pos": dif_pos,
            "chamber_idx": chamber_idx,
            "geo_idx": geo_idx,
            "is_valid": int(is_valid)
        }
        self.executeCmd(difCmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def removeSlotFromDb(self, slot_id, geo_idx):
        ''' You need to remove reference to this slot in `difs` before you can call this function
            Remove slot_id from `chambers` with given geo_idx
            slot_id should start from 0
            raise a ValueError if slot_id not found or slot_id still linked to a dif
        '''
        # Check slot is not in used in `difs`
        chamber_idx = self.getChamberIdx(slot_id=slot_id, geo_idx=geo_idx)
        if chamber_idx is None:
            errMsg = "Can't remove slot '%s' associated with geometry '%s' " % (slot_id, self.getGeometryName(geo_idx))
            logger.error(errMsg)
            raise ValueError(errMsg)

        checkUsed = 'SELECT `num` FROM `difs` WHERE (`chamber_idx`=%(chamber_idx)s AND `geometry_idx`=%(geo_idx)s)'
        args = {"chamber_idx": chamber_idx, "geo_idx": geo_idx}
        is_used = self.executeCmd(checkUsed, args, fetch_all=False)

        if is_used is not None:
            errMsg = "Can't remove slot '%s'! It is still linked to dif '%s'" % (slot_id, is_used['num'])
            logger.error(errMsg)
            raise ValueError(errMsg)

        slotCmd = 'DELETE FROM `chambers` WHERE (`num`=%(slot_id)s AND `geometry_idx`=%(geo_idx)s)'
        args = {"slot_id": slot_id, "geo_idx": geo_idx}
        self.executeCmd(slotCmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def removeDifFromDb(self, dif_id, geo_idx, slot_id=None):
        ''' Remove row associated with dif_id/geo_idx from `difs`
            if a slot_id is given, will only try to remove the dif if the associated slot_id corresponds
            raise ValueError if slot_id not found, or unable to remove dif (due to wrong given slot_id in parameters)
        '''
        if slot_id is not None:
            chamber_idx = self.getChamberIdx(slot_id=slot_id, geo_idx=geo_idx)
            if chamber_idx is None:
                errMsg = "Can't remove dif '%s' associated with geometry '%s' " % (dif_id,
                                                                                   self.getGeometryName(geo_idx))
                logger.error(errMsg)
                raise ValueError(errMsg)
            difCmd = 'DELETE FROM `difs` WHERE (`num`=%(dif_id)s AND `chamber_idx`=%(chamber_idx)s AND `geometry_idx`=%(geo_idx)s)'
            args = {"dif_id": dif_id, "chamber_idx": chamber_idx, "geo_idx": geo_idx}
        else:
            difCmd = 'DELETE FROM `difs` WHERE (`num`=%(dif_id)s AND `geometry_idx`=%(geo_idx)s)'
            args = {"dif_id": dif_id, "geo_idx": geo_idx}

        self.executeCmd(difCmd, args, is_insert=True)
        geo_name = self.getGeometryName(geo_idx)

        # Check it's done (might not be the case if the wrong slot_id was given))
        checkCmd = 'SELECT `chamber_idx` FROM `difs` WHERE (`num`=%(dif_id)s and `geometry_idx`=%(geo_idx)s)'
        args = {"dif_id": dif_id, "geo_idx": geo_idx}
        chamber_idx = self.executeCmd(checkCmd, args, fetch_all=False)
        if chamber_idx is not None:
            slotIdCmd = 'SELECT `num` FROM `chambers` WHERE (`idx`=%(chamber_idx)s and `geometry_idx`=%(geo_idx)s)'
            args = {"chamber_idx": chamber_idx['chamber_idx'], "geo_idx": geo_idx}
            dif_slot_id = self.executeCmd(slotIdCmd, args, fetch_all=False)
            errMsg = "Dif '%s' was not removed from geometry '%s', check the associated slots: You said '%s', found '%s' in db" % (
                dif_id, geo_name, slot_id, dif_slot_id)
            logger.error(errMsg)
            raise ValueError(errMsg)

        logger.info("Successfully removed dif '%s' from geometry '%s'" % (dif_id, geo_name))

    # -----------------------------------------------------------------------------
    def insertNewSlotIntoDb(self, slot_id, slot_pos, dif_left, dif_center, dif_right, tb_idx, geo_idx):
        cmd = 'INSERT INTO `chambers` (`num`, `x0`, `y0`, `z0`, `x1`, `y1`, `z1`, `left`, `center`, `right`, `tb_idx`, `geometry_idx`, `valid`) VALUES (%(slot_id)s, 0, 0, %(z0)s, 100, 100, %(z0)s, %(left)s, %(center)s, %(right)s, %(tb_idx)s, %(geo_idx)s, 1)'
        args = {
            "slot_id": slot_id,
            "z0": slot_pos,
            "left": dif_left,
            "center": dif_center,
            "right": dif_right,
            "tb_idx": tb_idx,
            "geo_idx": geo_idx
        }
        self.executeCmd(cmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def insertNewDifIntoDb(self, dif_id, dif_pos, slot_id, tb_idx, geo_idx, is_valid=True):
        ''' Insert new row in `difs`
            If is_valid=False is given, data from this dif will be skipped in the analysis
            raise ValueError if slot_id not found
        '''
        chamber_idx = self.getChamberIdx(slot_id=slot_id, geo_idx=geo_idx)
        if chamber_idx is None and is_valid:
            errMsg = "Can't insert dif '%s' associated with geometry '%s' " % (dif_id, self.getGeometryName(geo_idx))
            logger.error(errMsg)
            raise ValueError(errMsg)
        difCmd = 'INSERT INTO `difs` (`num`, `DI`, `DJ`, `POLI`, `POLJ`, `chamber_idx`, `tb_idx`, `geometry_idx`, `valid`) VALUES (%(dif_id)s, 0, %(dif_pos)s, 1, 1, %(chamber_idx)s, %(tb_idx)s, %(geo_idx)s, %(is_valid)s)'
        args = {
            "dif_id": dif_id,
            "dif_pos": dif_pos,
            "chamber_idx": chamber_idx,
            "tb_idx": tb_idx,
            "geo_idx": geo_idx,
            "is_valid": int(is_valid)
        }
        self.executeCmd(difCmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def insertNewGeometry(self, slotList, tb_name, geo_name, difToSkipList=None, slot_width=2.8):
        ''' Insert a new Geometry into the db
            Expect slotList to be list of {'slot':slot_id, 'left':leftDifId, 'center':centerDifId, 'right':rightDifId}
        '''
        if difToSkipList is None:
            difToSkipList = []
        # check geometry name doesn't exist yet for the current tb
        tb_idx = self.getTestBeamIdx(tb_name)
        if tb_idx is None:
            errMsg = "TB '%s' doesn't exist" % tb_name
            logger.error(errMsg)
            raise ValueError(errMsg)

        already_exist = self.getGeometryIdx(geo_name, tb_idx, silent=True)

        if already_exist is not None:
            logger.warning("Geometry '%s' already exists for testBeam '%s', call updateGeometry to change it" %
                           (geo_name, tb_name))
            return

        cmd = 'INSERT INTO `geometry_versions` (`name`, `tb_idx`, `valid`) VALUES (%(geo_name)s, %(tb_idx)s, 1)'
        args = {"geo_name": geo_name, "tb_idx": tb_idx}
        self.executeCmd(cmd, args, is_insert=True)

        # Check it was successfully added
        geo_idx = self.getGeometryIdx(geo_name, tb_idx)

        difList = []
        for slot in slotList:
            slot_id = slot['slot']
            left = slot.get('left', None)
            center = slot.get('center', None)
            right = slot.get('right', None)
            z0 = int(slot_id) * slot_width

            self.insertNewSlotIntoDb(slot_id, z0, left, center, right, tb_idx, geo_idx)
            if left is not None:
                difList.append(left)
                self.insertNewDifIntoDb(dif_id=left, dif_pos=0, slot_id=slot_id, tb_idx=tb_idx, geo_idx=geo_idx)
            if center is not None:
                difList.append(center)
                self.insertNewDifIntoDb(dif_id=center, dif_pos=32, slot_id=slot_id, tb_idx=tb_idx, geo_idx=geo_idx)
            if right is not None:
                difList.append(right)
                self.insertNewDifIntoDb(dif_id=right, dif_pos=64, slot_id=slot_id, tb_idx=tb_idx, geo_idx=geo_idx)

        for dif in difToSkipList:
            self.insertNewDifIntoDb(dif_id=dif,
                                    dif_pos=None,
                                    slot_id=None,
                                    tb_idx=tb_idx,
                                    geo_idx=geo_idx,
                                    is_valid=False)

        # Check all chambers/difs have been added
        if len(slotList) != len(self.getListOfSlot(geo_idx)):
            errMsg = "WRONG: Number of slot in geometry file (%s) != number of slot in db (%s)" % (
                len(slotList), len(self.getListOfSlot(geo_idx)))
            logger.error(errMsg)
            raise ValueError(errMsg)

        if len(difList) != len(self.getListOfDif(geo_idx)):
            errMsg = "WRONG: Number of dif in geometry file (%s) != number of dif in db (%s)" % (
                len(difList), len(self.getListOfDif(geo_idx)))
            logger.error(errMsg)
            raise ValueError(errMsg)

        logger.info("Successfully added geometry '%s' for tb '%s'" % (geo_name, tb_name))

    # -----------------------------------------------------------------------------
    def removeGeometry(self, tb_name, geo_name):
        tb_idx = self.getTestBeamIdx(tb_name)
        geo_idx = self.getGeometryIdx(geo_name=geo_name, tb_idx=tb_idx)
        # Check no run associated with geometry
        cmd = 'SELECT `run` FROM `logbook` WHERE (`geo_idx`=%s)'
        args = geo_idx
        run_list = self.executeCmd(cmd, args)
        if run_list is not None:
            errMsg = "Can not remove geometry '%s' of testbeam '%s' because the following runs are still associated with it: %s" % (
                geo_name, tb_name, run_list)
            logger.error(errMsg)
            raise ValueError(errMsg)

        # Remove difs
        cmd = 'DELETE FROM `difs` WHERE (`geometry_idx`=%s)'
        args = geo_idx
        self.executeCmd(cmd, args, is_insert=True)

        # Remove chambers
        cmd = 'DELETE FROM `chambers` WHERE (`geometry_idx`=%s)'
        args = geo_idx
        self.executeCmd(cmd, args, is_insert=True)

        # Remove geometry
        cmd = 'DELETE FROM `geometry_versions` WHERE (`idx`=%s)'
        args = geo_idx
        self.executeCmd(cmd, args, is_insert=True)

    # -----------------------------------------------------------------------------
    def getEnergyFromRun(self, run_number):
        '''Get energy associated with run_number
        '''
        cmd = 'SELECT `energy` FROM `logbook` WHERE (`run`=%s)'
        args = run_number
        ans = self.executeCmd(cmd, args, fetch_all=False)
        if ans is not None:
            return ans['energy']
        logger.error("Could not find energy associated with run '%s' " % run_number)

    # -----------------------------------------------------------------------------
    def getFilesFromRun(self, run_number, file_type=None, location_type=None, is_compressed=False):
        '''Return list all filePaths associated with run_number
        '''
        fPathList = []

        # Notice the missing closing brace
        cmd = 'SELECT `location` FROM `files` WHERE (`run`=%(run_number)s AND `compress`=%(is_zip)s'
        args = {"run_number": run_number, "is_zip": is_compressed}

        if file_type is not None:
            analysis_idx = self.getAnalysisIdx(analysis_type=file_type)
            cmd += ' AND `analysis_idx`=%(analysis_idx)s'
            args.update({"analysis_idx": analysis_idx})
        if location_type is not None:
            location_idx = self.getLocationIdx(location_type=location_type)
            cmd += ' AND `location_idx`=%(location_idx)s'
            args.update({"location_idx": location_idx})

        # Now we can add the closing brace
        cmd += ')'

        for file in self.executeCmd(cmd, args):
            if file is not None:
                fPathList.append(file['location'])
        if fPathList is not None:
            return fPathList
        logger.error("Could not find files associated with run '%s' " % run_number)

    # -----------------------------------------------------------------------------
    def getFileNameFromRun(self, run_number, isCompressed=False):
        '''Return list of all the fileNames associated with run_number
        '''
        return list(os.path.basename(file) for file in self.getFilesFromRun(run_number, isCompressed))

    # -----------------------------------------------------------------------------
    def dumpDifList(self, geo_idx):
        ''' Dump list of dif in TestBeam db
        '''
        rows = self.getRowsOfDif(geo_idx)
        logger.info("\n\n--------------- Dumping Dif List ---------------")
        logger.info("------------------------------------------------")
        logger.info(" - - - - - - - - - - - - - - - - - - - - - - - -")
        logger.info("|   Dif   \t| Layer\t|   X\t|   Y   \t|")
        logger.info("| - - - - - - - - - - - - - - - - - - - - - - - |")
        for row in rows:
            logger.info("|  %s   \t| %s \t| %s \t| %s \t|" %
                        (row['dif_id'], row['slot_id'] + 1, row['posJ'], row['posI']))
        logger.info("| - - - - - - - - - - - - - - - - - - - - - - - |")

    # -----------------------------------------------------------------------------
    def dumpslotList(self, geo_idx):
        ''' Dump list of chamber in TestBeam db
        '''
        rows = self.getRowsOfSlot(geo_idx)
        logger.info("--- Dumping Chambers ---")
        logger.info(" - - - - - - - - - - - - - - - -")
        logger.info("| Layer\t|   X\t|   Y\t|   Z\t|")
        logger.info("| - - - - - - - - - - - - - - - |")
        for row in rows:
            logger.info("|  %s \t| %s \t| %s \t| %s \t|" % (row['slot_id'] + 1, row['x0'], row['y0'], row['z0']))
        logger.info(" - - - - - - - - - - - - - - - -")

    # -----------------------------------------------------------------------------
    def getNumberOfEventsFromFiles(self, files_path, path_to_lcio_bin=None):
        ''' Call `lcio_event_counter` on the given files to extract number of events
            files_path can be a list of paths, string of paths separated by spaces or a single path
            if path_to_lcio_bin is not given, will try to find the exec from $LCIO env variable (as set by init_ilcsoft.sh)
            raise IOError if exec is not found
            returns nEvts found in file or None if the event counter was not able to run

        '''
        # Find lcio install if path_to_lcio_bin not given
        if path_to_lcio_bin is None:
            path_to_lcio_bin = os.getenv('LCIO')
            if path_to_lcio_bin is not None:  # env var is set
                path_to_lcio_bin += '/bin/'
        found_lcio = os.path.exists(path_to_lcio_bin)
        if not found_lcio:
            err_msg = "Could not find `lcio_event_counter` in '%s', please set ${LCIO} env variable(e.g by sourcing init_ilcsoft.sh)  or pass the proper path to the LCIO binary folder in the function call" % path_to_lcio_bin
            logger.error(err_msg)
            raise IOError(err_msg)

        if not isinstance(files_path, list):
            files_path = files_path.split()

        try:
            found_evts = int(subprocess.check_output([path_to_lcio_bin + 'lcio_event_counter'] + files_path).strip())
        except subprocess.CalledProcessError:  # at least one file can't be processed by lcio, try to find wich one
            found_evts = None
            for file_path in files_path:
                try:
                    found_evts += subprocess.check_output([path_to_lcio_bin + 'lcio_event_counter', file_path])
                except subprocess.CalledProcessError:
                    logger.error("`lcio_event_counter` is not able to process '%s' " % file_path)
            return None

        return found_evts

    # -----------------------------------------------------------------------------
    def getNumberOfEventsFromLogbook(self, run_number):
        ''' Get number of events in run as reported in the logbook
            return None if value is not set
        '''
        cmd = 'SELECT `n_evt` FROM `logbook` WHERE (`run`=%(run_number)s)'
        args = {'run_number': run_number}
        db_evts = self.executeCmd(cmd=cmd, args=args, fetch_all=False)
        if db_evts is None:
            logger.error("Number of events is not set in logbook for run '%s'`" % run_number)
            return None

        return db_evts['n_evt']

    # # -----------------------------------------------------------------------------
    # def getNumberOfEvents(self, run_number, file_type, location_type):
    #     ''' Get number of events in the `files` table for the given run/file_type/location_type
    #         return None if value is not set
    #     '''
    #     analysis_idx = self.getAnalysisIdx(analysis_type=file_type)
    #     location_idx = self.getLocationIdx(location_type=location_type)
    #     cmd = 'SELECT `events` FROM `files` WHERE (`run`=%(run_number)s AND `analysis_idx`=%(analysis_idx)s AND `location_idx`=%(location_idx)s)'
    #     args = {'run_number': run_number, 'analysis_idx': analysis_idx, 'location_idx': location_idx}
    #     db_evts = self.executeCmd(cmd=cmd, args=args, fetch_all=False)
    #     if db_evts is None:
    #         logger.error("Number of events is not set in table `files` for run '%s'`, file type '%s' on '%s'." %
    #                      (run_number, file_type, location_type))
    #         return None

    #     return db_evts['events']

    # -----------------------------------------------------------------------------
    def updateNumberOfEvents(self, run_number, file_type, location_type, n_evts):
        ''' Update `events` field in the `files` table
        '''
        analysis_idx = self.getAnalysisIdx(analysis_type=file_type)
        location_idx = self.getLocationIdx(location_type=location_type)
        cmd = 'UPDATE `logbook` SET `n_evt`=%(n_evts)s WHERE (`run`=%(run)s)'
        args = {'n_evts': n_evts, 'run_number': run_number}
        return self.executeCmd(cmd=cmd, args=args, is_insert=True)

    # -----------------------------------------------------------------------------
    def checkNumberOfEvents(self, run_number, file_type, location_type, expected_evts=None):
        ''' Check number of events reported in db is the same as expected
            if expected_evts is not given, will try to call lcio_event_counter to get number of expected events.
            file_type (raw, trivent, etc.) and location_type (eos_sdhcal, grid, etc.) need to be defined in the `analysis_version` and `files_locations_types` tables respectively
            return tuple of 3 params: (bool, int,int):
                - db_evts==expected_evts
                - number of evts found in db
                - number of evts found by lcio or expected_evts if set (for consistency in the number of params returned)
            return False, None, None if no events found in db
        '''
        db_evts = self.getNumberOfEventsFromLogbook(run_number=run_number)
        if db_evts is None:
            return False, None, None

        if expected_evts is not None:
            return db_evts == expected_evts, db_evts, expected_evts

        files_path = self.getFilesFromRun(run_number=run_number, file_type=file_type, location_type=location_type)
        found_evts = self.getNumberOfEventsFromFiles(files_path)

        if found_evts != db_evts:
            logger.warning("`lcio_event_counter` found '%s', db has '%s' evts." % (found_evts, db_evts))

        return found_evts == db_evts, db_evts, found_evts

    # -----------------------------------------------------------------------------
    def updateLogBook(self, run_number, change_dict=None):
        '''
            Return number of entry updated, None if no entry found for run_number
        '''
        if not change_dict:
            change_dict = {}
        entry_exists = self.executeCmd('SELECT `run` FROM `logbook` WHERE (`run`=%s)', run_number)
        if not entry_exists:
            logger.error("No entry found for run %s in logbook" % run_number)
            return None

        valid_keys = [
            'run', 'daq', 'configuration', 'date', 'energy', 'beam', 'thresh0', 'thresh1', 'n_evt', 'absorb', 'hv',
            'gas', 'pos', 'geo_idx', 'tb_idx', 'cer_idx', 'comment', 'valid'
        ]

        update_str = ''
        args = {}
        for k, v in change_dict.items():
            if k not in valid_keys:
                logger.error("Key '%s' is not valid, you can only update the following keys: %s" % (k, str(valid_keys)))
                return None
            update_str += '`{key}`=%({key})s,'.format(key=k)
            args.update({str(k): v})
            logger.debug(update_str % args)

        cmd = 'UPDATE `logbook` SET {} WHERE (`run`=%(run)s)'.format(update_str.strip(','))
        return self.executeCmd(cmd=cmd, args=args, is_insert=True)

    # -----------------------------------------------------------------------------
    def insertNewLocationType(self, location_type):
        ''' Add entry in `files_locations_types` table
        '''
        cmd = 'INSERT INTO `files_locations_types` (`name` , `valid`) VALUES (%s , 1)'
        return self.executeCmd(cmd=cmd, args=location_type.lower(), is_insert=True)

    # -----------------------------------------------------------------------------
    def insertNewAnalysisType(self, location_type):
        ''' Add entry in `analysis_versions` table
        '''
        cmd = 'INSERT INTO `analysis_versions` (`name` , `valid`) VALUES (%s, 1);'
        return self.executeCmd(cmd=cmd, args=location_type.lower(), is_insert=True)

    # -----------------------------------------------------------------------------
    def insertRunFile(self,
                      run_number,
                      path_to_file,
                      file_type,
                      location_type,
                      force_name=False,
                      add_missing_type=False,
                      check_path=True,
                      commit=False):
        '''
            add entry in `files` for run_number associated with file_type (raw, streamout, trivent, etc.)
            path_to_file is the full absolute path (eg. /path/to/file/name_run_number.slcio)
            function checks that run_number is in the filename and exit if not
            You can set force_name to True to disable this check
            location_type as defined in `files_locations_types` table corresponds to physical location of the file (on eos, grid, local, lyonas, etc)
            file_type as defined in `analysis_versions` table : Raw file, Streamout, Trivent etc.
            add_missing_type: if location or file type doesn't exists in db add it., otherwise throw an error
            check_path: Check file exists before committing, set to false when adding grid file since it won't be accessible to this script
            return number of file added
        '''
        if str(run_number) not in path_to_file:
            logger.error('Mismatching run_number in file name. You want to register %s with run %s' %
                         (path_to_file, run_number))
            raise ValueError

        analysis_idx = self.getAnalysisIdx(file_type)
        if add_missing_type and not analysis_idx:
            logger.warning("File type %s does not exists in db, adding it." % file_type)
            analysis_idx = self.insertNewAnalysisType(file_type)

        loc_idx = self.getLocationIdx(location_type)
        if add_missing_type and not loc_idx:
            logger.warning("Location type %s does not exists in db, adding it." % location_type)
            loc_idx = self.insertNewLocationType(location_type)

        exists_cmd = 'SELECT `location` FROM `files` WHERE (`run`=%(run_number)s AND `analysis_idx`=%(analysis_idx)s AND `location_idx`=%(location_idx)s)'
        exists_args = {'run_number': run_number, 'analysis_idx': analysis_idx, 'location_idx': loc_idx}
        already_exists = self.executeCmd(cmd=exists_cmd, args=exists_args)
        if already_exists:
            files = [f['location'] for f in already_exists]
            for f in files:
                if f == path_to_file:
                    logger.warning('File path %s already registered in db for run %s on %s with type %s' %
                                   (path_to_file, run_number, location_type, file_type))
                    return None
            logger.warning('Files already registered in db for run %s on %s with type %s: %s' %
                           (run_number, location_type, file_type, str(files)))

        if check_path is True:
            if not os.path.exists(path_to_file):
                logger.error('File %s not found. run: %s location_type: %s file_type: %s' %
                             (path_to_file, run_number, location_type, file_type))
                return None

        cmd = 'INSERT INTO `files` (`location`, `compress`, `run`, `analysis_idx`, `location_idx`, `valid`) VALUES (%(location)s, %(compress)s, %(run)s, %(analysis_idx)s, %(location_idx)s, %(valid)s)'
        args = {
            'location': path_to_file,
            'compress': False,
            'run': run_number,
            'analysis_idx': analysis_idx,
            'location_idx': loc_idx,
            'valid': True
        }
        if commit is True:
            return self.executeCmd(cmd=cmd, args=args, is_insert=True)
        else:
            logger.info(cmd % args)

    # -----------------------------------------------------------------------------
    def getLocationIdx(self, location_type, is_valid=True):
        '''
            get idx in `files_locations_types` table for location_type
            location_type is transformed to lower()
        '''
        cmd = 'SELECT `idx` FROM `files_locations_types` WHERE (`name`=%(location_type)s AND `valid`=%(is_valid)s)'
        args = {'location_type': location_type.lower(), 'is_valid': is_valid}
        idx = self.executeCmd(cmd=cmd, args=args, fetch_all=False)
        if idx is not None:
            return idx['idx']
        else:
            logger.warning('File location type index not found for type %s' % location_type.lower())
            location_list = self.executeCmd('SELECT `name` FROM `files_locations_types` WHERE (`valid`=%(is_valid)s)',
                                            {'is_valid': is_valid})
            if location_list is not None:
                logger.warning('Available types are :')
                for location in location_list:
                    logger.warning(location['name'])
            else:
                logger.warning('No analysis available')

    # -----------------------------------------------------------------------------
    def getAnalysisIdx(self, analysis_type, is_valid=True):
        '''
            get idx in `analysis_versions` table for analysis_type
            analysis_type is transformed to lower()
        '''
        cmd = 'SELECT `idx` FROM `analysis_versions` WHERE (`name`=%(analysis_type)s AND `valid`=%(is_valid)s)'
        args = {'analysis_type': analysis_type.lower(), 'is_valid': is_valid}
        idx = self.executeCmd(cmd=cmd, args=args, fetch_all=False)
        if idx is not None:
            return idx['idx']
        else:
            logger.warning('Analysis index not found for type %s' % analysis_type.lower())
            analysis_list = self.executeCmd('SELECT `name` FROM `analysis_versions` WHERE (`valid`=%(is_valid)s)',
                                            {'is_valid': is_valid})
            if analysis_list is not None:
                logger.warning('Available types are :')
                logger.warning(analysis_list)
            else:
                logger.warning('No analysis available')

    # -----------------------------------------------------------------------------
    def getRunList(self, conditions_dict=None):
        ''' build a list of runs respecting the conditions given
            possible conditions are: ['run_range', 'daq', 'state', 'configuration', 'date_range' 'tb_name', 'geo_name', 'energy', 'beam', 'thresh0', 'thresh1', 'n_evt', 'absorb', 'hv', 'gas', 'pos', 'geo_idx', 'tb_idx', 'cer_idx', 'is_valid']
            ranges are treated as inclusive
            Configuration is the full state name (eg. Dome_42chambres_Reference_v4_158)
            state is just the state number (eg 158)
        '''
        if conditions_dict is None:
            conditions_dict = {}
        valid_keys = [
            'run_range', 'daq', 'state', 'configuration', 'date_range', 'tb_name', 'geo_name', 'energy', 'beam',
            'thresh0', 'thresh1', 'n_evt', 'absorb', 'hv', 'gas', 'pos', 'geo_idx', 'tb_idx', 'cer_idx', 'is_valid'
        ]

        conditions = ''
        args = {}
        for k, v in conditions_dict.items():
            if k not in valid_keys:
                logger.error("Key '%s' is not valid, you can only use the following keys: %s" % (k, str(valid_keys)))
                return None

            # If there is more than one conditions
            if conditions:
                conditions += ' AND '

            # if the value is a list, treat it as a range condition
            if not isinstance(v, list):
                if k == 'state':  # state is a special case
                    conditions += '`configuration` LIKE %(state)s'
                    args.update({'state': "%\_" + str(v)})

                elif 'name' in str(k):
                    if 'tb' in k:
                        tb_idx = self.getTestBeamIdx(v)
                        conditions += '`tb_idx` = %(tb_idx)s'
                        args.update({'tb_idx': tb_idx})
                    elif 'geo' in k:
                        # Need to also get tb_name, will see about that later(TM)...
                        logger.warning('Selection from geo_name is not implemented yet')
                        # geo_idx = self.getGeometryIdx(v)
                        # conditions += '`geo_idx` = %(geo_idx)s'
                        # args.update({'geo_idx': geo_idx})
                else:
                    conditions += '`{key}`=%({key})s'.format(key=k)
                    args.update({str(k): v})
            else:
                if len(v) != 2:
                    raise ValueError(
                        '%s need to have a lower and upper limit, you provided a list with %s element: %s' %
                        (k, len(v), v))
                key = k.replace("_range", '')
                if 'date' in key:
                    # logger.debug('date_range: %s -> %s ' % (v[0], v[1]))
                    date_start = self.parse_date(v[0])
                    date_end = self.parse_date(v[1])
                    # logger.debug('date_range: %s -> %s ' %(date_start, date_end))
                    conditions += 'DATE(`{key}`) BETWEEN %(date_start)s AND %(date_end)s'.format(key=key)
                    args.update({'date_start': date_start, 'date_end': date_end})
                else:
                    conditions += '`{key}` BETWEEN %({key}_lower)s AND %({key}_upper)s'.format(key=key)
                    args.update({'{key}_lower'.format(key=key): v[0], '{key}_upper'.format(key=key): v[1]})

        cmd = 'SELECT `run` FROM `logbook` WHERE ( {conditions} )'.format(conditions=conditions)
        runList = self.executeCmd(cmd=cmd, args=args)
        # logger.warning(runList)
        return [run['run'] for run in runList] if runList is not None else None

    # -----------------------------------------------------------------------------
    def parse_date(self, date, format='%Y-%m-%d %H:%M:%S'):
        '''
            check date format is ok before injecting in sql
            default format is '%Y-%m-%d %H:%M:%S'
            return string with date properly formatted for sql ('%Y-%m-%d %H:%M:%S')
        '''
        try:
            dt = datetime.strptime(date, format)
            return datetime.strftime(dt, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            logger.error("Expect date to be formatted as %s" % format)
            return None
