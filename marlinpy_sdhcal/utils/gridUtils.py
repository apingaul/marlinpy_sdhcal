#!/usr/bin/env python
'''
    Most function won't probably work, tis file is just copy/pasta from various scripts I made to save them.
'''
# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import str

import subprocess
import platform
import time
import os

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__name__)

from DIRAC.Core.Base import Script
# Can't call Script.parseCommandLine() as recommended by Dirac people. Otherwise can't call gridUtils in our own
# executable with command line since parseCommandLine would parse the args intended for our executable...
Script.initialize(enableCommandLine=False)

from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from DIRAC.Resources.Catalog.FileCatalog import FileCatalog
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import (Marlin, GenericApplication)

# if marlinDict['Global']['runOnGrid'] is True:
#     # Will send a local copy of the folder to the Worker Node
#     gridUtils = GridUtils()
#     gridUtils.makeArxiv('./lib', 'myProcessorLib.tar.gz')
#     gridUtils.makeArxiv('./lib', 'processor.tar.gz', ['./.git', './.vscode', './build', './lib', './*.pyc', './*.yml'])
#     # upload the lib to the grid
#     dirac-dms-remove-files /calice/users/i/initial/some/path/lib.tar.gz
#     dirac-dms-add-file /calice/users/i/initial/some/path/lib.tar.gz lib.tar.gz CERN-SRM
#     dirac-dms-replicate-lfn /calice/users/i/initial/some/path/lib.tar.gz DESY-SRM
#     dirac-dms-replicate-lfn /calice/users/i/initial/some/path/lib.tar.gz IN2P3-SRM
#     job.setInputSandbox("LFN:/calice/users/i/initial/some/path/lib.tar.gz")


# -----------------------------------------------------------------------------
def getDiracInstance(withRepo=False, repoLocation=''):
    return DiracILC(withRepo=withRepo, repoLocation=repoLocation)


# -----------------------------------------------------------------------------
def createJob(application,
              lfns,
              input_data,
              output_path,
              name='',
              group='',
              log_level='info',
              SE='IN2P3-USER',
              output_sandbox=None,
              input_sandbox=None,
              platform='Linux_x86_64_glibc-2.17'):
    ''' Create Dirac job
    '''
    logger.debug("Creating new job with name:'%s'  in group: '%s' " % (name, group))
    job = UserJob()
    job.setName(name)
    job.setJobGroup(group)
    job.setLogLevel(log_level)
    logger.debug("lfns: '%s'" % lfns)
    logger.debug("OutputPath: '%s'" % output_path)
    logger.debug("SE: '%s'" % SE)
    # Need to strip trailing '/' from outputPath if present since job will fail otherwise
    job.setOutputData(lfns=lfns, OutputPath=output_path.rstrip('/'), OutputSE=SE)
    logger.debug("inputData: '%s'" % input_data)
    logger.debug("inputSandbox: '%s'" % input_sandbox)
    logger.debug("outputSandbox: '%s'" % output_sandbox)
    job.setInputData(input_data)
    if input_sandbox is not None:
        job.setInputSandbox(input_sandbox)
    if output_sandbox is not None:
        job.setOutputSandbox(output_sandbox)
    job.append(application)
    job.dontPromptMe()
    # not compatible with ilcDirac even if platform listed in dirac-platforms
    # Libraries need to be compiled on slc6 for now...
    # job.setPlatform(platform)
    return job


# -----------------------------------------------------------------------------
def configureDiracMarlin(steering_file,
                         processor_lib_list,
                         input_file,
                         log_file,
                         output_file=None,
                         extra_cli_args='',
                         ilcsoft_version="ILCSoft-2019-07-09_gcc62"):
    ''' Returns marlin app
        Set output_file only if chaining processors, use job.setOutputData in any case

    '''

    logger.debug("Creating new Marlin application with :")
    logger.debug("ilcsoft_version : '%s' - steering_file: '%s' - processors: '%s' - inputFile: '%s'" %
                 (ilcsoft_version, steering_file, processor_lib_list, input_file))
    logger.debug("extra_cli_args : '%s'" % extra_cli_args)

    marlin = Marlin()
    marlin.setVersion(ilcsoft_version)
    marlin.setSteeringFile(steering_file)
    if not isinstance(processor_lib_list, list):
        processor_lib_list = list(processor_lib_list)
    marlin.setProcessorsToUse(processor_lib_list)
    marlin.setExtraCLIArguments(extra_cli_args)
    marlin.setInputFile(input_file)
    marlin.setLogFile(log_file)
    if output_file is not None:
        marlin.setOutputFile(output_file)
    return marlin


# -----------------------------------------------------------------------------
def makeArxiv(folderToArxiv, arxivName, excludeList=None):
    '''Make a tgz arxiv from folderToArxiv skipping excludeList
    '''
    if excludeList is None:
        excludeList = []
    dir, folder = os.path.split(folderToArxiv.rstrip('/'))
    cmd = "tar -C " + dir
    for f in excludeList and excludeList:
        cmd += " --exclude " + f
    cmd += " -zcvf " + arxivName + " " + folder
    # logger.debug(cmd + [a for a in args])
    realCmd = cmd.split(' ')
    logger.info("Making arxiv of folder '%s' with cmd '%s'" % (folderToArxiv, realCmd))
    try:
        subprocess.check_output(realCmd)
    except subprocess.CalledProcessError as e:
        logger.error('Failed to create the archive', exc_info=True)


# -----------------------------------------------------------------------------
def getFileCatalog():
    ''' Instantiate and return the Dirac catalog
    '''
    fc = FileCatalog()
    # For some reason the fc instantiation fails most of the tine on the first time...Need to wait a bit then retry and all is ok
    if not fc.isOK():
        time.sleep(1)
        fc = FileCatalog()
        if not fc.isOK():
            logger.error("Can't access FileCatalog, make sure your grid_proxy is still valid")
            return None
    return fc


# -----------------------------------------------------------------------------
def listFilesInGridDirectory(dir_path):
    ''' Return list of files in directory
        return None if a problem occurred
    '''
    fc = getFileCatalog()
    if fc is None:
        logger.error('Could not instantiate fc')
        return None

    if not fc.isDirectory(dir_path)['Value']['Successful'][dir_path]:
        logger.error("Directory '%s' not found on the grid" % dir_path)
        return None
    return fc.listDirectory(dir_path)['Value']['Successful'][dir_path]['Files'].keys()


# -----------------------------------------------------------------------------
def findFileOnGrid(grid_file, userPath=None):
    ''' Look into the dirac fileCatalog for grid_file
        if file has relative path, prepend userPath before checking
        return tuple of (bool, str)
        return (False, errMsg) if error occurs (file catalog not available, userPath not defined, etc.)
        return (True, '') if file exists
        return (False, '') if file doesn't exist
    '''
    fc = getFileCatalog()
    if fc is None:
        return False, "Could not instantiate the file catalog"

    if grid_file[0] != '/':  # relative path to user folder
        if userPath is None:
            errMsg = "Looking for file '%s' on grid: Path is relative to your home folder on the grid, you must define it in the variable 'userPath' of the grid section configuration (should be of the form '/calice/users/i/initial')" % grid_file
            logger.error(errMsg)
            return False, errMsg
        # Ensure userPath ends with a '/'
        grid_file = userPath.rstrip('/') + '/' + grid_file
    ans = fc.isFile(grid_file)
    logger.debug(ans)
    return (True, '') if ans['Value']['Successful'][grid_file] else (False, '')
