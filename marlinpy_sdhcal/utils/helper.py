#!/usr/bin/env python

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
import six

import os
import sys
import time
import platform
import importlib
import subprocess
import json
from lxml import etree
from collections import OrderedDict

import logging
if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    logger = logging.getLogger(__name__)

try:
    from marlinpy_sdhcal.utils import gridUtils as gdu
except ImportError:
    logger.warning(
        "Could not import Dirac, check your PYTHONPATH or source dirac env file if you plan on running on the grid")


# -----------------------------------------------------------------------------
def importConfig(configFilePath):
    '''
    '''
    if not os.path.isfile(configFilePath):
        raise IOError(logger.error("configFile '%s' not found." % configFilePath))

    configDir, configFile = os.path.split(configFilePath)
    configDir = os.path.abspath(configDir)

    try:
        sys.path.append(configDir)  # Allow import of configfile in nested subfolder
        return importlib.import_module(configFile.replace('.py', ''))
    except (ImportError, SyntaxError):
        logger.error("Cannot import config file '%s'" % configFile)
        raise


# -----------------------------------------------------------------------------
def elapsedTime(startTime):
    ''' Print time to finish job
    '''
    t_sec = round(time.time() - startTime)
    (t_min, t_sec) = divmod(t_sec, 60)
    (t_hour, t_min) = divmod(t_min, 60)
    logger.info('Time passed: %.0f hour %.0f min %.0f sec' % (t_hour, t_min, t_sec))


# -----------------------------------------------------------------------------
def xmlValueBuilder(rootHandle,
                    xmlHandleName,
                    value,
                    xmlHandleType='parameter',
                    parameterType=None,
                    option=None,
                    optionValue=None,
                    xmlParList=None):
    '''
    '''
    xmlHandle = etree.SubElement(rootHandle, xmlHandleType, name=xmlHandleName)
    if parameterType is not None:
        xmlHandle.set("type", parameterType)
    if option is not None:
        xmlHandle.set(option, optionValue)
    xmlHandle.text = value
    if xmlParList is not None:
        xmlParList[xmlHandleName] = [value]


# -----------------------------------------------------------------------------
def generateXMLGeometryFile(xmlFileName, difList, layerList):
    ''' answer from the db is formated as a list (representing the rows) of dict (as columnName:Value))
        We need to extract the info we want and reformat it properly

        For the first xml section (<setup_geom>) we want :
        DifId, LayerId, PosX, posY, ShiftX, ShiftY \n
        DifId, LayerId, PosX, posY, ShiftX, ShiftY \n etc.

        for the second xml section (<ChamberGeom>) we want :
        LayerId, x0, y0,z0, ShiftZ

        <setup_geom>
            <parameter name="DifGeom">
                DifId, LayerId, PosX, posY, ShiftX, ShiftY
                ...
            </parameter>
            <parameter name="ChamberGeom">
                LayerId, x0, y0,z0, ShiftZ
                ...
            </parameter>
        </setup_geom>
    '''

    difToPrintList = []
    for row in difList:
        difToPrintList.append(','.join(
            str(rowLayer)
            for rowLayer in [row['dif_id'], row['slot_id'] +
                             1, row['posI'], row['posJ'], row['shiftI'], row['shiftJ']]))
    difToPrint = '\n'.join(difToPrintList)
    rootHandle = etree.Element('setup_geom')
    xmlValueBuilder(rootHandle, "DifGeom", difToPrint)

    layerToPrintList = []
    for row in layerList:
        layerToPrintList.append(','.join(
            str(rowLayer) for rowLayer in [row['slot_id'] + 1, row['x0'], row['y0'], row['z0'], '0.0']))
    layerToPrint = '\n'.join(layerToPrintList)
    xmlValueBuilder(rootHandle, "ChamberGeom", layerToPrint)

    # pretty string
    s = etree.tostring(rootHandle, pretty_print=True)
    with open(xmlFileName, "w") as outFile:
        outFile.write(s)


class CompactJSONEncoder(json.JSONEncoder):
    """
        Custom json encoder to create a nice looking json config file
        adapted from https://gist.github.com/jannismain/e96666ca4f059c3e5bc28abb711b5c92
        A JSON Encoder that puts small lists on single lines.
    """
    def customsort(self, dict1, key_order):
        '''
            properly order the json file
            Taken from https://stackoverflow.com/a/51471920
        '''
        items = [dict1[k] if k in dict1.keys() else None for k in key_order]
        sorted_dict = OrderedDict()
        for i in range(len(key_order)):
            if items[i] is not None:
                sorted_dict[key_order[i]] = items[i]
        return sorted_dict

    MAX_WIDTH = 72
    """Maximum width of a Single Line List (SLL)."""

    indentation_level = 0
    """Maximum number of items of a Single Line List (SLL)."""
    def encode(self, o):
        """Encode JSON object *o* with respect to single line lists."""
        if isinstance(o, (list, tuple)):
            if self._is_single_line_list(o):
                return "[" + ", ".join(json.dumps(el) for el in o) + "]"
            else:
                self.indentation_level += 1
                output = [self.indent_str + self.encode(el) for el in o]
                self.indentation_level -= 1
                return "[\n" + ",\n".join(output) + "\n" + self.indent_str + "]"
        elif isinstance(o, dict):
            self.indentation_level += 1
            if 'slot' in o:
                key_order = ['slot', 'left', 'center', 'right']
                sorted_dict = self.customsort(o, key_order)
                temp_output = self.indent_str + '{ '
                for k, v in sorted_dict.items():
                    temp_output += ('"' + str(k) + '": ' + str(v) + ', ')
                output = temp_output.strip(', ') + ' }'
                self.indentation_level -= 1
                return output
            else:
                output = [self.indent_str + json.dumps(k) + ':' + self.encode(v) for k, v in o.items()]
            self.indentation_level -= 1
            return "{\n" + ",\n".join(output) + "\n" + self.indent_str + "}"
        else:
            return json.dumps(o)

    def _is_single_line_list(self, o):
        # return len(o) <= self.MAX_ITEMS and len(str(o)) - 2 <= self.MAX_WIDTH
        return len(str(o)) - 2 <= self.MAX_WIDTH

    @property
    def indent_str(self):
        return " " * self.indentation_level * self.indent


# -----------------------------------------------------------------------------
def generateJsonGeometryFile(fName, difRow, chambersOutList=None, difsToSkipList=None, bifId=3):
    ''' answer from the db is formated as a list (representing the rows) of dict (as columnName:Value))
        We need to extract the info we want and reformat it properly to:
    {
    "chambers":
    [
        {"slot": slotId  , "left":leftDifId  , "center":centerDifId  , "right":rightDifId } ,
        ...
    ],
    "chambersOut": = Slots we don't want to include in the analysis but are present in the data
    [
        {"slot": slotId  , "left":leftDifId  , "center":centerDifId  , "right":rightDifId } ,
        ...
    ],
    "difsToSkip": = Difs we don't want to include in the analysis but are present in the data
    [
        DifId,
        ...
    ],
    "bifId":bifId = difNumber for the BIF, default to 3 if not found in the db
    }
    '''
    if chambersOutList is None:
        chambersOutList = []
    if difsToSkipList is None:
        difsToSkipList = []

    data = {'chambers': [], 'chambersOut': chambersOutList, 'difsToSkip': difsToSkipList, 'bifId': bifId}
    for row in difRow:
        data['chambers'].append({k: v for k, v in row.items()})

    with open(fName, 'w') as outfile:
        s = CompactJSONEncoder(indent=2).encode(data)
        outfile.write(s)  # Can't use json.dump in this case for proper formating


# -----------------------------------------------------------------------------
def generateGeometryFileFromDb(db, tbName=None, geoName=None, runNumber=None, filePath='./', format='json'):
    '''
        Return path to geom file
    '''
    if tbName is None and geoName is None:
        tbIdx = db.getTestBeamIdxFromRun(runNumber)
        if tbIdx is None:
            return None
        tbName = db.getTestBeamNameFromIdx(tbIdx)
        geoIdx = db.getGeometryIdxFromRun(runNumber)
        if geoIdx is None:
            errMsg = "No geometry associated with run '%s' found in db" % runNumber
            logger.error(errMsg)
            raise ValueError(errMsg)
        geoName = db.getGeometryName(geoIdx)
    else:
        tbIdx = db.getTestBeamIdx(tbName)
        geoIdx = db.getGeometryIdx(geo_name=geoName, tb_idx=tbIdx)

    logger.debug("Selected Geometry '%s' from TestBeam: '%s', index: '%s'" % (geoName, tbName, geoIdx))
    dirName = os.path.dirname(filePath)
    fName, fExt = os.path.splitext(os.path.basename(filePath))

    if fExt != '.' + format:  # filePath is weird, discard the name but keep the directory
        fName = None

    if not fName:
        if runNumber is not None:
            fName = 'geometry_run' + str(runNumber)
        else:
            fName = 'geometry_' + tbName + '_' + geoName
    if not fName.lower().endswith(('.' + format)):
        fName += '.' + format

    # Force fullPath to be unicode on py2...
    fullPath = '/'.join([dirName, fName])
    if format == 'json':
        cmd = 'SELECT `num` AS `slot`, `left`, `center`, `right` FROM `chambers` WHERE (`geometry_idx`=%s)'
        args = geoIdx
        difList = db.executeCmd(cmd, args)
        skipCmd = 'SELECT `num` FROM `difs` WHERE  (`geometry_idx`=%s AND `valid`=0)'
        difSkipList = [dif['num'] for dif in db.executeCmd(skipCmd, args)]
        generateJsonGeometryFile(fName=fullPath, difRow=difList, difsToSkipList=difSkipList)
    elif format == 'xml':
        difList = db.getRowsOfDif(geoIdx)
        layerList = db.getRowsOfLayer(geoIdx)
        generateXMLGeometryFile(fullPath, difList, layerList)
    else:
        logger.error("Can only generate xml or json geometry file")
    return fullPath


# -----------------------------------------------------------------------------
def checkJsonGeometry(layerList):
    '''
        Returns layerList
        Check if the format is good: expect layerList to be list of {'slot':slot_id, 'left':leftDifId, 'center':centerDifId, 'right':rightDifId}
        raise a KeyError if missing slot, but set dif_id to None if missing left,center or right
        Keep the possibility to have less than 3 difs in a layer
        Check for duplicate layer/difs.
        raise ValueError with list of duplicates if it finds some
    '''
    slotList = []
    difList = []
    idx = -1
    for layer in layerList:
        idx += 1
        slotList.append(layer['slot'])
        for dif in ['left', 'center', 'right']:
            try:
                difList.append(layer[dif])
            except KeyError as _:
                logger.warning("Missing dif in %s section of layer %i" % (dif, layer['slot']))

    uniqueLayers = set(slotList)
    uniqueDifs = set(difList)

    if len(uniqueLayers) == len(slotList):  # No layer dup
        if len(uniqueDifs) == len(difList):  # All is good
            logger.debug("Json geometry file is good to go")
            return layerList
    else:
        for layer in uniqueLayers:
            slotList.remove(layer)
    if len(uniqueDifs) != len(difList):
        for dif in uniqueDifs:
            difList.remove(dif)

    err_msg = "Found duplicates in geometry! dif: %s - slot: %s" % (str(difList), str(slotList))
    logger.error(err_msg)
    raise ValueError(err_msg)


# -----------------------------------------------------------------------------
def insertNewGeometryIntoDb(db, jsonFile, tbName, geoName=None):
    ''' Insert a new Geometry into the db
    if geomName is not given will use the jsonFile name
    '''
    fName, fExt = os.path.splitext(os.path.basename(jsonFile))
    if fExt != '.json':
        raise IOError(logger.error("Geometry file should be in a json format, found '%s'" % fExt))

    if not geoName:
        geoName = fName

    geoDict = json.load(open(jsonFile))
    layerList = checkJsonGeometry(geoDict['chambers'])
    difToSkipList = geoDict['difsToSkip']
    db.insertNewGeometry(layerList, tbName, geoName, difToSkipList=difToSkipList)


# -----------------------------------------------------------------------------
def updateGeometryInDb(db, jsonFile, tbName, geoName):
    _, fExt = os.path.splitext(os.path.basename(jsonFile))
    if fExt != '.json':
        raise IOError(logger.error("Geometry file should be in a json format, found '%s'" % fExt))

    geoDict = json.load(open(jsonFile))
    layerList = checkJsonGeometry(geoDict['chambers'])
    difToSkipList = geoDict['difsToSkip']
    db.updateGeometry(layerList, tbName, geoName, difToSkipList=difToSkipList)


# -----------------------------------------------------------------------------
def removeGeometryFromDb(db, tbName, geoName):
    db.removeGeometry(tb_name=tbName, geo_name=geoName)


# -----------------------------------------------------------------------------
def getPeriodNameFromRun(runNumber):
    if runNumber >= 715371 and runNumber < 715788:
        return 'SPS_08_2012'

    if runNumber >= 716197 and runNumber < 726177:
        return 'SPS_11_2012'

    if runNumber >= 726177 and runNumber < 727760:
        return 'SPS_12_2014'

    if runNumber >= 727760 and runNumber <= 728456:
        return 'SPS_04_2015'

    if runNumber >= 728501 and runNumber <= 728682:
        return 'PS_06_2015'

    if runNumber >= 730436 and runNumber <= 730926:
        return 'SPS_10_2015'

    if runNumber >= 730927 and runNumber <= 732909:
        return 'SPS_06_2016'

    if runNumber >= 733626 and runNumber <= 733759:
        return 'SPS_10_2016'

    if runNumber >= 736500 and runNumber <= 736575:
        return 'SPS_09_2017'

    if runNumber >= 743500 and runNumber <= 745000:
        return 'SPS_09_2018'


# -----------------------------------------------------------------------------
def checkPeriod(runNumber, runPeriod, configFile):
    ''' Check runNumber is associated to correct runPeriod
        Abort execution if not
        TODO: Automate with db check
    '''
    class PeriodError(ValueError):
        def __init__(self, goodPeriod):
            goodPeriod = goodPeriod
            ValueError.__init__(
                self, "RunNumber '{}' is from TestBeam '{}', you selected '{}' in configFile '{}'".format(
                    runNumber, goodPeriod, runPeriod, configFile))

    if runNumber >= '715371' and runNumber <= '715788' and runPeriod != 'SPS_08_2012':
        raise PeriodError('SPS_08_2012')

    if runNumber >= '716197' and runNumber <= '726177' and runPeriod != 'SPS_11_2012':
        raise PeriodError('SPS_11_2012')

    if runNumber >= '726177' and runNumber <= '726414' and runPeriod != 'SPS_12_2014':
        raise PeriodError('SPS_12_2014')

    if runNumber >= '727760' and runNumber <= '728456' and runPeriod != 'SPS_04_2015':
        raise PeriodError('SPS_04_2015')

    if runNumber >= '728501' and runNumber <= '728682' and runPeriod != 'PS_06_2015':
        raise PeriodError('PS_06_2015')

    if runNumber >= '730436' and runNumber <= '730926' and runPeriod != 'SPS_10_2015':
        raise PeriodError('SPS_10_2015')

    if runNumber >= '730927' and runNumber <= '732909' and runPeriod != 'SPS_06_2016':
        raise PeriodError('SPS_06_2016')

    if runNumber >= '733626' and runNumber <= '733759' and runPeriod != 'SPS_10_2016':
        raise PeriodError('SPS_10_2016')

    if runNumber >= '736500' and runNumber <= '736575' and runPeriod != 'SPS_09_2017':
        raise PeriodError('SPS_09_2017')

    if runNumber >= '743500' and runNumber <= '745000' and runPeriod != 'SPS_09_2018':
        raise PeriodError('SPS_09_2018')


# -----------------------------------------------------------------------------
def rsyncFile(runNumber, fName, serverName, serverPath, localPath, rsyncOptions='azpv', shouldPrintProgress=True):
    ''' Download file from serverName:serverPath to localPath
        rsyncOptions set to -azpv by default to optimize transfer
    '''
    logger.info("Downloading run '%s' from %s:%s" % (str(runNumber), serverName, serverPath))
    fName.format(runNumber)
    remotePath = serverName + ":" + serverPath + fName.format(runNumber)
    rsyncCmd = ['rsync']
    rsyncCmd.append('-' + rsyncOptions)
    if shouldPrintProgress is True:
        rsyncCmd.append('--info=progress2')
    rsyncCmd.append(remotePath)
    rsyncCmd.append(localPath)
    try:
        logger.debug('Downloading file with command:')
        logger.debug(rsyncCmd)
        subprocess.check_call(rsyncCmd)
    except (subprocess.CalledProcessError, OSError) as _:
        logger.error("Something wrong happened while downloading with rsync: return code", exc_info=True)
        raise


# -----------------------------------------------------------------------------
def findAllInputFiles(fileFormatList, run_number, isOnGrid=False):
    '''
        Return list of file found given a list of file formats
    '''
    # Assume that if not a list, you meant to pass a list of 1 item...
    if not isinstance(fileFormatList, list):
        inFileList = findFilesWithFormat(fileFormatList, isOnGrid)
        if inFileList is None:
            logger.error("No file found for format '%s'" % fileFormatList)
            return None
        return [f for f in inFileList if str(run_number) in f]

    inFileList = []
    for f in fileFormatList:
        fList = findFilesWithFormat(f, isOnGrid)
        if fList is not None:
            inFileList += fList

    if inFileList is None:
        logger.error("No file found for formats '%s'" % fileFormatList)
        return None
    return [f for f in inFileList if str(run_number) in f]


# -----------------------------------------------------------------------------
def findFilesWithFormat(fileFormat, isOnGrid=False):
    '''
        Find all input files available for a given run following the lcio naming scheme
        lcio add '_digit' at the end of a file when cutting it in pieces
    '''
    # Extract base file name and extension
    dirName, fName = os.path.split(fileFormat)
    fName, fExt = os.path.splitext(fName)
    # check if file name ends with a digit separated by a '_', return the list of files following the pattern
    # Convoluted, Need refactor -> Need to check case where file doesn't end with digit but still match the format (eg. toto_0.slcio, toto_0_retoto.slcio)
    if (fName.split('_')[-1]).isdigit():
        format = fName.strip(fName.split('_')[-1])
        if not isOnGrid:
            fList = [f for f in os.listdir(dirName) if (format in f and fExt in f)]
            fList.sort()
            return map(lambda s: dirName + '/' + s, fList)
        else:
            try:
                fList = [f for f in gdu.listFilesInGridDirectory(dirName) if (format in f and fExt in f)]
            except TypeError:  # listFilesInGridDirectory return None
                logger.error('No file found in directory %s' % dirName)
                return None
            fList.sort()
            # Unlike os.path.listdir the dirac output for listdir returns full path to file, no need to concatenate the dir_name in this case
            return fList

        # There is only one file associated with this run, no need to list the dir, still check file exists
        if not checkFileExists(fileFormat)[0]:
            logger.error("Could not find file '%s'" % fileFormat)
        return [fileFormat]


# -----------------------------------------------------------------------------
def replaceExpressionInDict(dic, expression, value):
    """
        Find and replace expression with value in the values of the dict dictionnary
    """
    for key, val in dic.items():
        if isinstance(val, dict):
            replaceExpressionInDict(val, expression, value)
        if isinstance(val, list):
            dic[key] = replaceExpressionInList(val, expression, value)
        if isinstance(val, six.string_types):
            dic[key] = replaceExpressionInString(val, expression, value)


# -----------------------------------------------------------------------------
def replaceExpressionInString(string, expression, value):
    if expression in string:
        # Can't use string.format() as there might be more than one expression (named argument) in the string...
        return string.replace('{' + expression + '}', str(value))
    else:
        return string


# -----------------------------------------------------------------------------
def replaceExpressionInList(a_list, expression, value):
    newList = []
    for element in a_list:
        if isinstance(element, six.string_types):
            newList.append(replaceExpressionInString(element, expression, value))
        else:
            newList.append(element)
    return newList


# -----------------------------------------------------------------------------
def checkFileExists(file, isOnGrid, gridUserPath=None):
    ''' Return (bool,errMsg).errMsg = '' if no error occurred
    '''
    if isOnGrid:
        return gdu.findFileOnGrid(file, gridUserPath)
    elif os.path.exists(file):
        return True, ''
    return False, ''


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
class InterceptHandler(logging.Handler):
    # https://github.com/Delgan/loguru/issues/69
    def emit(self, record):
        # Retrieve context where the logging call occurred, this happens to be in the 6th frame upward
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelname, record.getMessage())
