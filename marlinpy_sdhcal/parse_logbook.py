#!/usr/bin/env python
'''
    Parse sdhcal logbook to json file:
    1 - Export logbook entries to xml (Go to `Find` then make a selection of entries)
    2 - Make sure Sort in reverse order is not checked
    Note that the script will fail(by design) on entries with non utf8 gibberish with the following error:
        lxml.etree.XMLSyntaxError: Input is not proper UTF-8, indicate encoding !
    That is notably the case for entries 23647, 23648 of SPS_10_2015
    Please remove them by hand editing the xml file, before running the script (delete everything within and including the <ENTRY></ENTRY> tag)
'''

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)

import platform
import re
from lxml import etree
from xml.sax.saxutils import unescape
from datetime import datetime
import json
import argparse

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)


def getPrettyString(etreeElement):
    my_string = etree.tostring(etreeElement, pretty_print=True, method="html", encoding='unicode')
    return escapeAll(my_string)


def escapeAll(my_string):
    return unescape(
        my_string.replace("&nbsp;", "").replace('<p>', '').replace('</p>', '').replace('<br />', '').replace(
            '<strong>', '').replace('</strong>', '').replace('<em>', '').replace('</em>', ''))


def cleanLine(line):
    # Clean the line
    bad_chars = ':(){}<>='
    rgx = re.compile('[%s]' % bad_chars)
    s = rgx.sub(' ', line)
    ss = re.sub("\s\s+", " ", s)
    return ss


def getState(line):
    ''' Return stateFound, sdhcal_run, state
    '''
    state = None
    stateFound = False
    sdhcal_run = True
    stateLineOpening = ['DB', 'DAQ', 'Config', 'Dome', 'h2']
    bad_state = ['gif', 'tricot', 'pbar', 'aegis', 'test']
    if line is None:
        return False, False, None
    for slo in (slo for slo in stateLineOpening if not stateFound):
        if slo in line:
            lineList = cleanLine(line).split(' ')
            for word in (word for word in lineList if not stateFound):
                for bad in bad_state:
                    if bad in word.lower():
                        sdhcal_run = False
                        return True, sdhcal_run, None  # Found state but its not from sdhcal so no state name
                if '_' in word:
                    state = word
                    stateFound = True
                    continue
    return stateFound, sdhcal_run, state


def fillCerenkovAttributes(textLine):
    cerLine = cleanLine(textLine).split(' ')
    if len(cerLine) < 2:  # line is just a cerenkov keyword, no data
        return None
    cerDic = {}

    # Some time missing space between cer name and gas...
    pressureIdx = 2
    gasTuple = ('he', 'n2', 'co2')
    if cerLine[0].endswith(gasTuple):
        for word in gasTuple:
            if word in cerLine[0]:
                cerDic['Name'] = cerLine[0].strip(word)
                cerDic['Gas'] = word
                pressureIdx = 1
    else:
        cerDic['Name'] = cerLine[0]
        cerDic['Gas'] = cerLine[1]

    particleIdx = pressureIdx + 1
    # Try to find the pressure, if line is not properly formatted pass. Info is still conserved in "fullLine"
    cerDic['Pressure'] = None
    lastChance = [word.strip('bar').strip('b') for word in cerLine]
    for word in lastChance:
        try:
            cerDic['Pressure'] = float(word)
            continue  # assume first float is the pressure...
        except ValueError:
            pass
    # if cerDic['Pressure'] is not None:
    #     if cerDic['Pressure'] < 0 or cerDic['Pressure'] >= 3:
    #         cerDic['Pressure'] = None

    # Try to find the particle type
    if len(cerLine) > particleIdx:
        pList = [
            'e', 'e+', 'e-', 'electron', 'electrons', 'positron', 'positrons', 'pi', 'pi+', 'pi-', 'pions', 'pion', 'p',
            'p+', 'p-', 'protons', 'proton', 'k', 'k+', 'k-', 'kaons', 'kaon'
        ]
        for word in cerLine[particleIdx:]:
            for p in pList:
                cerDic['ParticleTag'] = word.strip('tag:') if p in word else ''
    cerDic['FullLine'] = textLine
    return cerDic


def main(args=None):
    '''
    '''

    parser = argparse.ArgumentParser(description='Parse sdhcal logbook from xml to json file', prog='parseLogBook')
    parser.add_argument('inputXmlLogBook', help='input xml logbook file')
    parser.add_argument('--outputJsonLogBook', help='output json logbook file, if not set will dump to terminal')
    args = parser.parse_args()

    parser = etree.XMLParser(recover=True, remove_blank_text=True, ns_clean=True, huge_tree=True, encoding="UTF-8")

    xml_data = etree.parse(args.inputXmlLogBook)

    root = xml_data.getroot()

    dataList = []
    for entry in root.iterfind(".//ENTRY"):
        runDic = {}
        runText = entry.find(".//Run").text
        # Don't even try to parse entry without a runNumber
        if runText is None:
            continue
        try:
            runNum = int(runText.strip(' '))
        except (ValueError, TypeError):
            logger.warning('Found a weird run: {}'.format(runText))
            continue
        if runNum == 0:
            continue

        runDate = entry.find(".//DATE").text
        if runDate is None:
            continue

        runDate = runDate.split(",")[1].split("+0")[0]
        runDate = datetime.strptime(runDate, " %d %b %Y %H:%M:%S ").strftime('%Y-%m-%d %H:%M:%S')
        energy = entry.find(".//Energie").text

        if energy is not None:
            energy = energy.lower().split('gev')[0].strip(' ')
            if energy.isdigit():
                energy = float(energy)
            else:
                energy = None

        runDic['elog_id'] = entry.find(".//MID").text

        runDic['runNumber'] = runNum
        runDic['runDate'] = runDate
        runDic['runDetector'] = entry.find(".//Detecteur").text
        runDic['runBeam'] = entry.find(".//Faisceau").text
        runDic['runEnergy'] = energy
        runDic['thresh0'] = entry.find(".//Seuil0").text
        runDic['thresh1'] = entry.find(".//Seuil1").text
        try:
            n_evts = int(entry.find(".//Evenements").text)
        except (
                ValueError,  # sometimes 'started' is written in this field instead of the number of triggers
                TypeError):  # int() Throw type error when nothing in field
            n_evts = None

        runDic['nEvts'] = n_evts
        runDic['absorb'] = entry.find(".//Absorbeur").text
        runDic['hvValue'] = entry.find(".//HV").text
        runDic['gas'] = entry.find(".//Gaz").text
        runDic['Pos'] = entry.find(".//PositionX").text
        dataOk = entry.find(".//OK").text
        if dataOk is not None:
            dataOk = True if 'Good' in str(etree.tostring(entry.find(".//OK"))) else False
        runDic['dataOk'] = dataOk

        # Parse comments...
        sdhcal_run = True
        stateFound = False
        if entry.find(".//TEXT").text is not None:
            comments = escapeAll(entry.find(".//TEXT").text).split("\n")
            # Iterate through each line to find the db state, stop looking once found
            for line in (line for line in comments if not stateFound):
                stateFound, sdhcal_run, runDic['state'] = getState(line)

        # In some tb the state was given in the detecteur field
        if not stateFound:
            stateFound, sdhcal_run, runDic['state'] = getState(runDic['runDetector'])
            if not stateFound:  # Give up
                runDic["state"] = None

        if not sdhcal_run:
            continue
        runDic["Comments"] = entry.find(".//TEXT").text
        cerAttr = {}
        if comments is not None:
            cerNameList = ['xcet', 'cerenkov', 'cherenkov', 'cedar']
            for line in comments:
                line = line.lower()
                if any(cer in line for cer in cerNameList):
                    line = line.strip('at')
                    cerAttr = fillCerenkovAttributes(line)
                    # logger.debug(runNum)
                    # logger.debug(cerAttr)

                    # runDic['Cerenkov']['Cer' + cerNum]
        runDic['Cerenkov'] = cerAttr
        dataList.append(runDic)

    if args.outputJsonLogBook is not None:
        with open(args.outputJsonLogBook, 'w') as outfile:
            logger.debug('writing to json')
            json.dump(dataList, outfile, ensure_ascii=False, indent=2)
    else:
        logger.info(json.dumps(dataList, ensure_ascii=False, indent=2))


if __name__ == "__main__":
    main()
