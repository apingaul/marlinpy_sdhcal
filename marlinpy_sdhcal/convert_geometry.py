#!/usr/bin/env python3
''' Small utility to convert old XML geometry files to json
    Requires python3! comment out the lines with logger if you really need to run with python2
    Old XML Format:
    <setup_geom>
        <parameter name="DifGeom">
            DifId, LayerId, PosX, posY, ShiftX, ShiftY
            ...
        </parameter>
        <parameter name="ChamberGeom">
            LayerId, x0, y0,z0, ShiftZ
            ...
        </parameter>
    </setup_geom>
    New json format:
    {
     "chambers":
    [
        {"slot":0  , "left":30  , "center":94  , "right":181 } ,
        ...
        {"slot":48 , "left":80  , "center":182 , "right":105 }
    ]

'''

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)

import os
import sys
import argparse
import platform
from lxml import etree
import json

from marlinpy_sdhcal.utils import helper
from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger(__file__)
    FORMAT = "[%(name)s:%(funcName)s: line %(lineno)s] - %(levelname)s - %(message)s"
    logging.basicConfig(format=FORMAT)
    logger.setLevel(logging.DEBUG)


def getDifPosLiteral(pos):
    if int(float(pos)) == 64:
        return 'right'
    elif int(float(pos)) == 32:
        return 'center'
    elif int(float(pos)) == 0:
        return 'left'
    else:
        raise ValueError(logger.exception('WTF is this position? {}'.format(pos)))


def readXMLGeometry(xmlFile):
    temp_dict = {}
    with open(xmlFile, "r") as inFile:
        tree = etree.parse(inFile)
        root = tree.getroot()
        try:
            dif_row = root.find("parameter[@name='DifGeom']").text.split('\n')
        except AttributeError as e:
            logger.error('DifGeom tag not found in geometry file {geo_file}'.format(geo_file=xmlFile))
            sys.exit(1)
        for row in dif_row:
            if row:  # Some row might be empty due to weird formatting
                split_row = row.split(',')
                if len(split_row) == 6:
                    logger.debug(split_row)
                    dif_id = int(split_row[0].strip(' '))
                    slot_id = int(split_row[1].strip(' ')) - 1
                    dif_pos = getDifPosLiteral(split_row[3].strip(' '))
                    temp_dict.setdefault(slot_id, []).append({'dif_id': dif_id, 'dif_pos': dif_pos})

        logger.debug(json.dumps(temp_dict, indent=2))

        slot_list = []
        for slot_id, dif_list in temp_dict.items():
            slot_difs = {difs['dif_pos']: difs['dif_id'] for difs in dif_list}
            # slot_dict = {"slot": slot_id, **slot_difs} // Not working in py2...
            slot_dict = {"slot": slot_id}
            slot_dict.update(slot_difs)
            slot_list.append(slot_dict)

        logger.debug(slot_list)
        return slot_list


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def main(args=None):
    parser = argparse.ArgumentParser(
        description='Convert old XML geometry file to json, if --jsonFile is not given will use the xml file name',
        prog='convertGeometry')
    parser.add_argument('xmlFile', help='xml geometry file')
    parser.add_argument('--jsonFile', help='json output name')
    parser.add_argument(
        '--rewriteJson',
        action='store_true',
        help='load a json geometry and rewrite it, useful to compare multiple geometries with different file formatting'
    )
    parser.add_argument('--logLevel', default='INFO', help='logger Level')
    args = parser.parse_args()

    logger.remove()
    logger.add(sys.stderr, level=args.logLevel)

    _, xmlExt = os.path.splitext(os.path.basename(args.xmlFile))
    if xmlExt != '.xml' and not args.rewriteJson:
        raise IOError(logger.exception("Geometry file should be in a xml format, found '{}'".format(xmlExt)))

    if args.jsonFile:
        fName = args.jsonFile.strip('.json') + '.json'
    else:
        fName = args.xmlFile.strip(xmlExt)
        if args.rewriteJson:
            fName += '_reordered'
        fName += '.json'
    logger.info("Will save geometry to '{jsonFile}'".format(jsonFile=fName))

    if not args.rewriteJson:
        slot_list = readXMLGeometry(args.xmlFile)
        helper.generateJsonGeometryFile(fName, slot_list, chambersOutList=[], difsToSkipList=[])
    else:
        geoDict = json.load(open(args.xmlFile))
        slot_list = helper.checkJsonGeometry(geoDict['chambers'])
        helper.generateJsonGeometryFile(fName, slot_list, chambersOutList=[], difsToSkipList=[])

    logger.info("Success!")


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    main()
