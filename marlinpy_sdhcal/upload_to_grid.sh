#!/usr/bin/env sh

source /cvmfs/clicdp.cern.ch/DIRAC/bashrc
file=$1
gridPath=$2
SE=$3

dirac-dms-add-file gridPath file SE
