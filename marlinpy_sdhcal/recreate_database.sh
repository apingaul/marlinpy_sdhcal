export LOGURU_LEVEL=INFO

root_folder="$(pwd)/../.."
sql_folder="${root_folder}/data/sqldata"
db_name="testGEOMETRY"
db_conf="${root_folder}/config/db.json"
db_port=1306
db_host=127.0.0.1
db_pass='RPC_2008'
mysql_cmd="mysql -h ${db_host} -P ${db_port} -u acqilc -p${db_pass} ${db_name}"
location="eos_antoine"
format="/eos/user/a/apingaul/CALICE/Data/{tb_name}/Raw/DHCAL_{run_number}_I0_0.slcio"
grid_path="/calice/users/a/apingaul/data/{tb_name}/Raw/"
SE='IN2P3-USER'
file_type="raw"

if [[ $(hostname) == *"cern.ch"* ]]; then
    mysql_cmd="mysql -h dbod-sdhcal-test.cern.ch -P 5506 -u acqilc -p${db_pass} ${db_name}"
    db_conf="${root_folder}/config/sdhcal_db.json"
    location="eos_sdhcal"
    format="/eos/project/s/sdhcal/data/{tb_name}/Raw/DHCAL_{run_number}_I0_0.slcio"
fi

declare -a testbeams=("SPS_08_2012"
    "SPS_11_2012"
    "SPS_12_2014"
    "SPS_04_2015"
    "PS_06_2015"
    "SPS_10_2015"
    "SPS_06_2016"
    "SPS_10_2016"
    "SPS_09_2018"
    "SPS_09_2017"
)

${mysql_cmd} -e "DROP DATABASE IF EXISTS ${db_name}; CREATE DATABASE ${db_name};"
${mysql_cmd} -e "source ${sql_folder}/emptyGeometry.sql"

for tb in "${testbeams[@]}"; do
    echo $tb
    ${mysql_cmd} -e "source ${sql_folder}/${tb}.sql"
done
echo 'Done'
${mysql_cmd} -e "source ${sql_folder}/addGeometryVersionning.sql"
echo 'Redone'

for tb in "${testbeams[@]}"; do
    xml=${root_folder}/data/ElogData/logbook_${tb}.xml
    json=${root_folder}/data/ElogData/logbook_${tb}.json
    python parseLogBook.py ${xml} --outputJsonLogBook ${json}
    python fillDBFromJson.py ${json} --db_conf ${db_conf}
done

# Register files on the grid
python insertFileIntoDb.py --db_conf ${db_conf} --format ${format} --location grid --type ${file_type} --gridPath ${grid_path} --runList 0 --SE ${SE}
# Register file on eos
python insertFileIntoDb.py --db_conf ${db_conf} --format ${format} --location ${location} --type ${file_type} --runList 0

unset LOGURU_LEVEL
