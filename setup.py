#!/usr/bin/env python

from setuptools import setup, find_packages


def get_version():
    g = {}
    exec(open("marlinpy_sdhcal/version.py").read(), g)
    return g["__version__"]


install_deps = [
    'future; python_version<"3"', 'pymysql[rsa]', 'argparse', 'lxml', 'six', 'pyparsing',
    'marlinpy @ git+https://gitlab.cern.ch/apingaul/marlinpy.git#egg=marlinpy', 'loguru; python_version>"3.5"',
    'gfal2-python; python_version<"3" and sys_platform=="linux"', 'gsi; python_version<"3" and sys_platform=="linux"'
]

test_deps = ['mock', 'pytest', 'tox']

setup(name='marlinpy_sdhcal',
      author='Antoine Pingault',
      author_email='antoine.pingault@cern.ch',
      maintainer='Antoine Pingault',
      maintainer_email='antoine.pingault@cern.ch',
      version=get_version(),
      description='marlinpy implementation for sdhcal',
      url='https://gitlab.cern.ch/apingaul/marlinpy_sdhcal',
      license='MIT',
      packages=find_packages(),
      install_requires=install_deps,
      extras_require={'testing': test_deps},
      python_requires=">=2.7",
      entry_points={
          'console_scripts': [
              'marlinpy_sdhcal = marlinpy_sdhcal.__main__:main',
              'convert_geometry = marlinpy_sdhcal.convert_geometry:main',
              'download_from_grid = marlinpy_sdhcal.download_from_grid:main', 'geo_db = marlinpy_sdhcal.geo_db:main',
              'insert_file_into_database = marlinpy_sdhcal.insert_file_into_database:main',
              'insert_logbook_into_database = marlinpy_sdhcal.insert_logbook_into_database:main',
              'parse_logbook = marlinpy_sdhcal.parse_logbook:main'
          ]
      },
      platforms="Any")
