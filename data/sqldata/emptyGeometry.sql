-- MySQL dump 10.13  Distrib 8.0.16, for osx10.14 (x86_64)
--
-- Host: localhost    Database: GEOMETRY
-- ------------------------------------------------------
-- Server version	8.0.16

 SET NAMES utf8 ;


--
-- Table structure for table `chambers`
--

DROP TABLE IF EXISTS `chambers`;
CREATE TABLE `chambers` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `num` smallint unsigned DEFAULT NULL,
  `x0` decimal(8 ,2) DEFAULT NULL,
  `y0` decimal(8 ,2) DEFAULT NULL,
  `z0` decimal(8 ,2) DEFAULT NULL,
  `x1` decimal(8 ,2) DEFAULT NULL,
  `y1` decimal(8 ,2) DEFAULT NULL,
  `z1` decimal(8 ,2) DEFAULT NULL,
  `left` smallint unsigned DEFAULT NULL,
  `center` smallint unsigned DEFAULT NULL,
  `right` smallint unsigned DEFAULT NULL,
  `geometry_idx` smallint unsigned DEFAULT NULL,
  `tb_idx` smallint unsigned DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `difs`
--

DROP TABLE IF EXISTS `difs`;
CREATE TABLE `difs` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `num` smallint unsigned DEFAULT NULL,
  `dI` decimal(5,2) DEFAULT NULL,
  `dJ` decimal(5,2) DEFAULT NULL,
  `polI` decimal(5,2) DEFAULT NULL,
  `polJ` decimal(5,2) DEFAULT NULL,
  `chamber_idx` smallint unsigned DEFAULT NULL,
  `geometry_idx` smallint unsigned DEFAULT NULL,
  `tb_idx` smallint unsigned DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `run` int unsigned DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `compress` tinyint(1) unsigned DEFAULT NULL,
  `analysis_idx` smallint unsigned DEFAULT NULL,
  `location_idx` smallint unsigned DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `analysis_versions`
--

DROP TABLE IF EXISTS `analysis_versions`;
CREATE TABLE `analysis_versions` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `valid` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `logbook`
--

DROP TABLE IF EXISTS `logbook`;
CREATE TABLE `logbook` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `mid` int unsigned DEFAULT NULL,
  `run` int unsigned DEFAULT NULL,
  `daq` varchar(256) DEFAULT NULL,
  `configuration` varchar(256) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `energy` decimal(7,2) DEFAULT NULL,
  `beam` varchar(256) DEFAULT NULL,
  `thresh0` varchar(256) DEFAULT NULL,
  `thresh1` varchar(256) DEFAULT NULL,
  `n_evt` int unsigned DEFAULT NULL,
  `absorb` varchar(256) DEFAULT NULL,
  `hv` decimal(7,2) DEFAULT NULL,
  `gas` varchar(256) DEFAULT NULL,
  `pos` varchar(256) DEFAULT NULL,
  `geo_idx` smallint unsigned DEFAULT NULL,
  `tb_idx` smallint unsigned DEFAULT NULL,
  `cer_idx` smallint unsigned  DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `valid` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `runs`
--

-- DROP TABLE IF EXISTS `runs`;
-- CREATE TABLE `runs` (
--   `idx` int unsigned NOT NULL AUTO_INCREMENT,
--   `quality` int DEFAULT NULL,
--   `debut` timestamp NULL DEFAULT NULL,
--   `fin` timestamp NULL DEFAULT NULL,
--   `run` int unsigned DEFAULT NULL,
--   `geometry_idx` smallint unsigned DEFAULT NULL,
--   `tb_idx` smallint DEFAULT NULL,
--   `valid` tinyint(1) DEFAULT '1',
--   PRIMARY KEY (`idx`)
-- ) ENGINE=InnoDB;

--
-- Table structure for table `testbeams`
--

DROP TABLE IF EXISTS `testbeams`;
CREATE TABLE `testbeams` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `quality` smallint DEFAULT NULL,
  `debut` timestamp NULL DEFAULT NULL,
  `fin` timestamp NULL DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `geometry_versions`
--

DROP TABLE IF EXISTS `geometry_versions`;
CREATE TABLE `geometry_versions` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `tb_idx` smallint unsigned DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `files_locations_types`
--

DROP TABLE IF EXISTS `files_locations_types`;
CREATE TABLE `files_locations_types` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;

--
-- Table structure for table `cerenkov_versions`
--

DROP TABLE IF EXISTS `cerenkov_versions`;
CREATE TABLE `cerenkov_versions` (
  `idx` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `gas` varchar(256) DEFAULT NULL,
  `pressure` decimal(4,2) DEFAULT NULL,
  `particle_tag` varchar(256) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB;


-- Dump completed
