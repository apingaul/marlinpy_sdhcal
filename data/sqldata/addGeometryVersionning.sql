INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_08_2012' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_08_2012") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_11_2012' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_11_2012") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_12_2014' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_12_2014") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_04_2015' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_04_2015") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('PS_06_2015'  , (SELECT `idx` FROM `testbeams` WHERE `name`="PS_06_2015")  , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_10_2015' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_10_2015") , 1);


INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_06_2016' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_06_2016") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_10_2016' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_10_2016") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_09_2017' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_09_2017") , 1);
INSERT INTO `geometry_versions` (`name` , `tb_idx` , `valid`) VALUES ('SPS_09_2018' , (SELECT `idx` FROM `testbeams` WHERE `name`="SPS_09_2018") , 1);

-- Add default geometry_idxx for each testBeam to difs/runs/chambers
UPDATE `difs`     AS d SET d.geometry_idx = (SELECT g.idx FROM `geometry_versions` AS g WHERE g.tb_idx = d.tb_idx);
-- UPDATE `runs`     AS r SET r.geometry_idx = (SELECT g.idx FROM `geometry_versions` AS g WHERE g.tb_idx = r.tb_idx);
UPDATE `chambers` AS c SET c.geometry_idx = (SELECT g.idx FROM `geometry_versions` AS g WHERE g.tb_idx = c.tb_idx);

UPDATE `chambers` AS c SET c.left   = (SELECT d.num FROM `difs` AS d WHERE (d.DJ = 0  AND d.chamber_idx = c.idx AND c.geometry_idx = d.geometry_idx));
UPDATE `chambers` AS c SET c.center = (SELECT d.num FROM `difs` AS d WHERE (d.DJ = 32 AND d.chamber_idx = c.idx AND c.geometry_idx = d.geometry_idx));
UPDATE `chambers` AS c SET c.right  = (SELECT d.num FROM `difs` AS d WHERE (d.DJ = 64 AND d.chamber_idx = c.idx AND c.geometry_idx = d.geometry_idx));

INSERT INTO `files_locations_types` (`name` , `valid`) VALUES ('eos_sdhcal' , 1);
INSERT INTO `files_locations_types` (`name` , `valid`) VALUES ('grid' , 1);
INSERT INTO `files_locations_types` (`name` , `valid`) VALUES ('eos_antoine' , 1);

INSERT INTO `analysis_versions` (`name` , `valid`) VALUES ('raw' , 1);
INSERT INTO `analysis_versions` (`name` , `valid`) VALUES ('streamout' , 1);
INSERT INTO `analysis_versions` (`name` , `valid`) VALUES ('trivent' , 1);
INSERT INTO `analysis_versions` (`name` , `valid`) VALUES ('trivent_ecal' , 1);
