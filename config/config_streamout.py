#!/usr/bin/env python
'''
    Configuration file for streamout processor, to use with marlinpy_sdhcal python package

    You provide a few mandatory paramaters (list of runs, path to the data, etc.) and the script will generate the default parameters associated with these runs. (geometry file, cerenkov parameters, etc.)
    There is a few assumptions made to generate these defaults parameters:
    - Input/Output data are stored in subdirectories under the same main directory with the following structure:
       - dataPath/runPeriod/{outputPath,inputPath}
    Every settings are stored in json objects, so you can individually change any and all of the parameters.
    Some global settings are stored in marlinDic (Grid, db, ilcsoft, marlin parameters template, etc.) and run specific settings in run_dict
'''

# Python 2/3 compatibility
from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import str

import os
import platform
import json
import copy

from marlinpy_sdhcal.utils import helper
from marlinpy_sdhcal.utils.dbUtils import DbUtils as dbu

if float(platform.python_version()[0:3]) >= 3.5:
    from loguru import logger
else:
    import logging
    logger = logging.getLogger("marlinpy_sdhcal")


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def getDefaultRunParameters(run_list=None, conf_dict=None, proc_name=None):
    run_dict = {}
    # Make the connection to the db outside the loop to avoid opening/closing it too many time
    if conf_dict['global']['useDb'] is True:
        db = dbu(db_conf=conf_dict['global']['dbConfig'])

    for run in run_list:
        if conf_dict['global']['useDb'] is True:
            run_period = db.getTestBeamNameFromRun(run)
            input_files = db.getFilesFromRun(run,
                                             file_type=conf_dict['global']['inputFileType'],
                                             location_type=conf_dict['global']['inputFilesLocation'])
            # TODO: Add matching of vo in filename. Might have files in /ilc/ and /calice/
        else:
            logger.debug("Looking for files outside of db...")
            run_period = helper.getPeriodNameFromRun(run)
            input_path = conf_dict['global']['inputPath'].replace("{runPeriod}", run_period).rstrip('/') + '/'
            logger.debug("InputPath='%s'" % input_path)
            full_input_format = [
                input_path + file.format(runNumber=run) for file in conf_dict['global']['inputFilesFormat']
            ]
            logger.debug("full_input_format='%s'" % full_input_format)
            input_files = helper.findAllInputFiles(full_input_format, run)

        if not input_files:
            logger.error("No input files found for run '{}'".format(run))
            continue

        # Input/output files in run_dict[run] are the full path to files
        # While files run_dict[run][marlin] are the local path to the files
        # Hence they are the same when running locally, but different when running on the grid
        run_dict[run] = {
            "runPeriod": run_period,
            "inputFiles": input_files,
            "outputPath": conf_dict['global']['outputPath'],
            "marlinpyCfgFile": conf_dict['global']['marlinpyCfgFileFormat'],
            "logFile": conf_dict['global']['logFileFormat'],
            "outputFiles": [
            ]  # Used to check if files already exists, filled automatically with LCIOOutputFile and ROOTOutputFile in filldefaultParameterPerRun
        }
    if not run_dict:
        logger.error("Can't run marlin as no input files were found for any run_number")
    return filldefaultParameterPerRun(run_dict, conf_dict, proc_name)


# -----------------------------------------------------------------------------
def filldefaultParameterPerRun(run_dict=None, config_dict=None, proc_name=None):

    for run in run_dict.keys():
        # Need to call deepcopy() to dissociate the two dict
        run_dict[run]['marlin'] = copy.deepcopy(config_dict['marlin'])

        runPeriod = run_dict[run]['runPeriod']
        if runPeriod.find("2012") != -1:
            run_dict[run]['marlin'][proc_name]['XDAQ_SHIFT'] = 94

        elif runPeriod.find("2014") != -1:
            run_dict[run]['marlin'][proc_name]['XDAQ_SHIFT'] = 24
            run_dict[run]['marlin'][proc_name]['CerenkovDifId'] = 1

        elif runPeriod.find("2015") != -1:
            run_dict[run]['marlin'][proc_name]['XDAQ_SHIFT'] = 24

        elif any(year in runPeriod for year in ['2016', '2017', '2018']):
            run_dict[run]['marlin'][proc_name]['XDAQ_SHIFT'] = 20
            run_dict[run]['marlin'][proc_name]['Before2016Data'] = False
            run_dict[run]['marlin'][proc_name]['TreatEcal'] = False
        else:
            raise ValueError("RunPeriod '%s' found for run '%s' doesn't exists..." % (runPeriod, str(run)))

        run_dict[run]['outputFiles'].append(run_dict[run]['marlin'][proc_name]['LCIOOutputFile'])
        # run_dict[run]['outputFiles'].append(run_dict[run]['marlin']['MyLCIOOutputProcessor']['LCIOOutputFile'])
        # run_dict[run]['outputFiles'].append(run_dict[run]['marlin'][proc_name]['ROOTOutputFile'])

        if config_dict['global']['runOnGrid'] is True:
            run_dict[run]['inputSandbox'] = [config_dict['global']['xmlFile'], config_dict['grid']['arxivName']]
            run_dict[run]['marlin'][proc_name]['LCIOOutputFile'] = os.path.basename(
                run_dict[run]['marlin'][proc_name]['LCIOOutputFile'])
            run_dict[run]['marlin']['global']['LCIOInputFiles'] = [
                os.path.basename(inFile) for inFile in run_dict[run]['inputFiles']
            ]
            run_dict[run]['repoLocation'] = config_dict['global']['repoLocation']
        else:
            run_dict[run]['marlin']['global']['LCIOInputFiles'] = run_dict[run]['inputFiles']

        helper.replaceExpressionInDict(run_dict[run], 'runNumber', run)
        helper.replaceExpressionInDict(run_dict[run], 'runPeriod', run_dict[run]['runPeriod'])

    return run_dict


# -----------------------------------------------------------------------------
def getDefaultGlobalParameters(conf_dict=None):
    '''
    '''
    # Automatically detect if running on lxplus and adapt ilcsoft installation parameters
    key = "local"
    if conf_dict['global']['runOnGrid'] is True:
        key = 'grid'
    elif 'lxplus' in platform.node():
        key = 'lxplus'

    data_path = conf_dict[key]['dataPath']
    glob_dict = conf_dict['global']
    proc_type = glob_dict['processorType']
    proc_path = glob_dict['processorPath']
    file_type = glob_dict['inputFileType']

    if key != 'grid':
        init_ilcsoft_script = conf_dict[key]['ilcsoftPath'] + "/" + conf_dict[key]['ilcsoftVersion'] + "/init_ilcsoft.sh"
    else:
        glob_dict['repoLocation'] = conf_dict['grid']['repoLocation']
        init_ilcsoft_script = ['dummy']

    # Can't use .format or % to replace string as there might be some format_keys not defined yet (such as {runPeriod} or {runNumber})
    helper.replaceExpressionInDict(glob_dict, 'procType', proc_type)
    helper.replaceExpressionInDict(glob_dict, 'procPath', proc_path)
    helper.replaceExpressionInDict(glob_dict, 'dataPath', data_path)
    helper.replaceExpressionInDict(glob_dict, 'inputFileType', file_type)

    glob_dict['inputFilesLocation'] = conf_dict[key]['inputFilesLocation']
    glob_dict['outputFilesLocation'] = conf_dict[key]['outputFilesLocation']
    glob_dict['initILCSoftScript'] = init_ilcsoft_script

    output_file = glob_dict['outputPath'] + '/' + glob_dict['outputFilesFormat']
    # conf_dict['marlin']['MyLCIOOutputProcessor']['LCIOOutputFile'] = output_file + '.slcio'
    conf_dict['marlin']['MyStreamoutProcessor']['LCIOOutputFile'] = output_file + '.slcio'

    # Replace shared lib extension on osx if running locally
    if platform.system().lower() == 'darwin' and not conf_dict['global']['runOnGrid']:
        glob_dict['marlinLibs'] = [os.path.splitext(lib)[0] + '.dylib' for lib in glob_dict['marlinLibs']]

    return conf_dict


# -------------------------------------------------------
# -------------------------------------------------------

# Use values in the local section by default
#  If the runOnGrid setting is set to `True` or scripts detects it's running on lxplus it will use values from the Grid section

# -------------------------------------------------------
##  STEP 1
##  Fill the template parameters to be applied for each run
# -------------------------------------------------------

conf_dict = {
    "grid": {
        "logLevel": "info",
        "userPath": "/calice/users/a/apingaul/",  # Path to your home folder on the grid, used to check for preexisting data
        "ilcsoftVersion": "ILCSoft-2019-07-09_gcc62",
        "dataPath": "data/{runPeriod}",  # path on grid are relative to /{VO}/user(s)/{user_initial}/{user_name}/
        "inputFilesLocation": "grid",  # Used with the db to filter where to find/store files. If you want to use files store in the sdhcal_eos folder use eos_sdhcal
        "outputFilesLocation": "grid",
        "storageElement": "IN2P3-USER",
        "inputSandbox": ["{xmlFile}", "{libArxiv}"],
        'arxivName': "libMarlin.tar.gz",
        "outputSandbox": ['*.log', '*.xml'],
        "repoLocation": "{procPath}/{procType}_{runPeriod}.rep",  # Where to store jobs
        # "backend": "Local"
        "backend": "wms"
    },
    "local": {
        "ilcsoftVersion": "x86_64-mac1014-clang100-opt",
        "ilcsoftPath": "/opt/ilcsoft/cvmfs/",
        "dataPath": "/eos/user/a/apingaul/CALICE/Data/{runPeriod}",
        "inputFilesLocation": "eos_antoine",
        "outputFilesLocation": "eos_antoine"
    },
    "lxplus": {
        "ilcsoftVersion": "x86_64-slc6-gcc62-opt",
        "ilcsoftPath": "/cvmfs/clicdp.cern.ch/iLCSoft/builds/2019-07-09/",
        "dataPath": "/eos/user/a/apingaul/CALICE/Data/{runPeriod}",
        "inputFilesLocation": "eos_sdhcal",
        "outputFilesLocation": "eos_antoine"
    },
    "global": {
        "runOnGrid": True,
        "useDb": True,  # Get info from db (inputFilePath, geometry)
        "logToFile": True,  # Save Marlin output to a file instead of terminal
        "processorType": "Streamout",  # Used for file naming (logs, configs, outputfiles), grid job naming. If useDb is True also used to determine the file type to register in `files`
        # "filesLocation": "eos_sdhcal",  # Used with the db to filter where to find/store files. If you want to use files store in the sdhcal_eos folder use eos_sdhcal
        "logFileFormat": "{dataPath}/Logs/{procType}_{runNumber}.log",  # Where to store the logs, {runNumber} and {procType} are needed but {dataPath} can be omitted to store the logs somewhere else.
        "dbConfig": "/eos/user/a/apingaul/CALICE/Software/marlinpy_sdhcal/config/db.json",
        "gridConfig": "/eos/user/a/apingaul/CALICE/Software/marlinpy_sdhcal/config/grid.json",
        "inputFileType": "Raw",  # used to determine inputPath from db
        "inputPath": "{dataPath}/{inputFileType}",  # if {dataPath} is present will be replaced by conf_dict['local/grid']['dataPath'] Ignored if useDb set to True
        "outputPath": "{dataPath}/Streamout/",  # if {dataPath} is present will be replaced by conf_dict['local/grid']['dataPath']
        "processorPath": '/eos/user/a/apingaul/CALICE/Software/Streamout/',
        # "inputFilesFormat": ['DHCAL_{runNumber}_SO_Antoine.slcio'],
        # "inputFilesFormat": ['DHCAL_{runNumber}_I0_0_fixed.slcio'],
        "inputFilesFormat": ['DHCAL_{runNumber}_I0_0.slcio'],
        "outputFilesFormat": 'DHCAL_{procType}_{runNumber}',  # {runNumber}, {procType} and file extension(slcio,root if needed) will be replaced accordingly
        # inputFilesFormat: You don't need to put all files associated with a run, in case the file listed ends with a digit (like the raw files)
        # script will look for other files with the same name scheme and add them to the list
        "xmlFile": "{procPath}/config/StreamoutProcessor.xml",  # Path to marlin steering file
        "marlinLibs": ["{procPath}/lib/marlin_dll/libStreamoutMarlin.so"],  # Marlin libraries to use
        "marlinpyCfgFileFormat": "{procPath}/config/marlinpyCfg/marlinCfg_{procType}_{runNumber}.yml"  # marlinpy needs a yml format config file, auto genereated by marlinpy_sdhcal
    }
}

marlin_dict = {
    'marlin': {  # Default Marlin parameters to change in marlin_steering file
        'global': {
            "Verbosity": "MESSAGE",
            "MaxRecordNumber": 0,  # Max Number of event to process
            "SkipNEvents": 0,  # Number of event to skip
            "LCIOInputFiles": []  # Autogenerated from conf_dict['inputFilesFormat']
        },
        'MyStreamoutProcessor': {
            "Verbosity": "MESSAGE",
            "InputCollectionName": 'RU_XDAQ',
            "OutputCollectionName": 'DHCALRawHits',
            # "LCIOOutputFile": "{outputPath}/{outputFilesFormat}.slcio",
            "LCIOOutputFile": "{outputPath}/{outputFilesFormat}.slcio",
            # "ROOTOutputFile": "{outputPath}/{outputFilesFormat}.root",
            # "PlotFolder": "{dataPath}/Plots/",  # Where to store plot with pdf format
            "RU_SHIFT": 23,
            "DropFirstRU": False,
            "XDAQ_SHIFT": 20,
            "CerenkovDifId": 3,
            "CerenkovOutDifId": 3,
            "CerenkovOutAsicId": 1,
            "CerenkovOutTimeDelay": 6,
            "SkipFullAsic": True,
            "Before2016Data": True,
            "TreatEcal": False,
            "EcalDetectorIds": '201 1100'
        }
    }
}

conf_dict.update(marlin_dict)
filled_conf_dict = getDefaultGlobalParameters(conf_dict=conf_dict)

# -------------------------------------------------------
## STEP 1.5
## If you are not happy with the auto generation of the config from step1...edit some value of this configuration if needed
# -------------------------------------------------------
# conf_dict['global']['geometryPath']= 'toto'
# conf_dict['global']['initILCSoftScript'] = "/path/to/alternate/init_ilcsoft.sh"

logger.debug("Configuration dict: \n {}".format(json.dumps(filled_conf_dict, indent=4)))

# -------------------------------------------------------
## STEP 2, Define the list of run to analyse, you can either provide your own list of run, or generate one corresponding to certain criteria from the db
## You can make a selection from a range of run/date or a specific value of the daq_name, daq_state, beam, etc.
## For now conditions can only be used as `AND`: i.e you cannot make a selection like run_range:[1,3] AND NOT state:12
# -------------------------------------------------------

# Auto generate from db
if conf_dict['global']['useDb']:
    # Connect to the db
    mydb = dbu(db_conf=conf_dict['global']['dbConfig'])
    bad_run_range = [72620, 72630]
    run_range = [728111, 728113]
    bad_daq_name = 'toto'
    daq_name = 'Dome_42chambres_Reference_v4_213'
    # runList = mydb.getRunList(conditions_dict={'run_range':bad_run_range})
    # runList = mydb.getRunList(conditions_dict={'run_range':run_range, 'state':163, 'beam':'pions', 'date_range':['2015-04-30 00:00:00','2015-05-01 00:00:00']})
    # runList = mydb.getRunList(conditions_dict={'date_range': ['2015-04-20 00:00:00', '2015-04-24 00:00:00']})
    runList = mydb.getRunList(conditions_dict={'tb_name': 'SPS_09_2018'})
    # runList = mydb.getRunList(conditions_dict={'state':163})

    # testbeams = mydb.executeCmd('SELECT `idx`,`name` FROM `testbeams`')
    # runList = []
    # for tb in testbeams:
    #     tb_idx = tb['idx']
    #     tb_name = tb['name']
    #     # if not 'SPS_04_2015' in tb_name:
    # runList += mydb.getRunList({'tb_idx': tb_idx})

# Provide your own
# runList = [
#     726252, 726280, 726307, 726310, 726328, 726335, 726337, 726356, 726360, 726362, 726372, 726373, 726374, 726377,
#     726403, 726407, 726409, 726410, 726412, 726413, 726414
# ]
# runList = [
#     run['run'] for run in mydb.executeCmd(
#         "select run from files where (location_idx=2 and run NOT IN (select run from files WHERE location_idx= 2 and analysis_idx=2))"
#     )
# ]
logger.info("runList: {}".format(runList))

# -------------------------------------------------------
## STEP 3:
##  Generate parameters for each run
# -------------------------------------------------------

run_dict = getDefaultRunParameters(runList, conf_dict=filled_conf_dict, proc_name='MyStreamoutProcessor')
logger.debug("runDict: \n {}".format(json.dumps(run_dict, indent=4)))
# Print global infos
# print(json.dumps(marlinDict, indent=2))
# Print all runs paramaters
# print(json.dumps(run_dict, indent=2))
